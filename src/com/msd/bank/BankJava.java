package com.msd.bank;

import com.msd.bank.scheduler.InterestAndPenaltyCalculationJob;
import com.msd.bank.scheduler.InterestAndPenaltyPaymentJob;
import com.msd.bank.util.Logger;
import com.msd.bank.view.HomeScreen;
import javax.swing.JFrame;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class BankJava {

    private static final Logger LOGGER = new Logger(BankJava.class);
    private static HomeScreen home;

    public static void main(String args[]) {
        setupScheduler();
        home = new HomeScreen();
        // Make sure the application gets closed when home screen is closed
        home.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        home.showForm();
    }
    
    
    private static void setupScheduler() {
        try {
            SchedulerFactory schedulerFactory = new org.quartz.impl.StdSchedulerFactory();
            Scheduler scheduler = schedulerFactory.getScheduler();
            
            // define interest and penalty calculation job
            JobDetail interestPenaltyCalculation = JobBuilder.newJob(InterestAndPenaltyCalculationJob.class)
                    .withIdentity("interestPenaltyCalculation")
                    .build();
            
            // define interest and penalty payment job
            JobDetail interestPenaltyPayment = JobBuilder.newJob(InterestAndPenaltyPaymentJob.class)
                    .withIdentity("interestPenaltyPayment")
                    .build();
            
            // Trigger for 14:00 every day
            Trigger dailyTrigger = TriggerBuilder.newTrigger()
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 00 14 * * ?"))
                    .build();
            
             // Trigger for 14:00 on the last day of every month
            Trigger monthlyTrigger = TriggerBuilder.newTrigger()
                    .withSchedule(CronScheduleBuilder.cronSchedule("0 00 14 L * ?"))
                    .build();
            
            // Tell quartz to schedule the job using our trigger
            scheduler.scheduleJob(interestPenaltyCalculation, dailyTrigger);
            scheduler.scheduleJob(interestPenaltyPayment, monthlyTrigger);
            scheduler.start();
        } catch (SchedulerException ex) {
            LOGGER.error("Error while creating scheduler", ex);
        }
    }
}
