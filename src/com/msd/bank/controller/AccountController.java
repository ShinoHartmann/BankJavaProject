package com.msd.bank.controller;

import com.msd.bank.util.InputValidationException;
import com.msd.bank.util.Logger;
import com.msd.bank.model.account.Account;
import com.msd.bank.model.account.AccountFactory;
import com.msd.bank.model.Customer;
import com.msd.bank.model.account.visitor.*;
import com.msd.bank.model.account.record.RecordHistoryComparator;
import com.msd.bank.model.account.record.Record;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang.time.DateUtils;
import static com.msd.bank.util.InputValidationUtils.*;

/**
 * This is the abstract controller class of BaseAccountController and LoanAccountController.
 * It contains methods related to both BaseAccount and LoanAccount.
 */
public abstract class AccountController {
    
    private static final Logger LOGGER = new Logger(AccountController.class);
    private final CustomerController customerController = CustomerController.getInstance();
    private final AccountStore accountStore = AccountStore.getInstance();
    private final HashMap<String, AccountFactory> accountFactories = new HashMap<>();
    
    /**
     * Factory design pattern is used to create accounts.
     * @param factories to store all the account factories by type
     * @param factory to create accounts for a specific account type
     */
    protected void registerAccountFactory(HashMap<String, AccountFactory> factories, AccountFactory factory) {
        factories.put(factory.getAccountType(), factory);
    }
    
    public void depositMoney(String accountId, String amount) {
        validateAccountIdAndAmount(accountId, amount);
        Account account = getAccount(accountId);
        account.depositMoney(new BigDecimal(amount));
    }
    
    public void transferMoney(String fromAccountId, String toAccountId, String amountInput) {
        validateAccountIdAndAmount(fromAccountId, toAccountId, amountInput);
        Account fromAcc = getAccount(fromAccountId);
        Account toAcc = getAccount(toAccountId);
        BigDecimal amount = new BigDecimal(amountInput);
        if (!fromAcc.hasSufficientFunds(amount)) {
            throw new InputValidationException("There are insufficient funds to make this transfer.");
        } 
        fromAcc.transferOut(amount);
        toAcc.transferIn(amount);
    }
    
    public void rollback(String accountId) {
        Account account = getAccount(accountId);
        account = account.rollback();
        accountStore.add(account);
    }
        
    /**
     * Gets all the account history and transactions of the specific account between fromDate and toDate.
     * @return collection of the account history and transactions.
     */
    public Collection<Record> getAccountRecords(String accountId, Calendar fromDate, Calendar toDate) {
        Collection<Record> records = new TreeSet<>(new RecordHistoryComparator());
        records.addAll(getTransactions(accountId, fromDate, toDate));
        records.addAll(getAccountHistory(accountId, fromDate, toDate));
        if(records.isEmpty()) {
            throw new InputValidationException("No account records found");
        }
        return records;
    }
    
    public Collection<Record> getTransactions(String accountId, Calendar fromDate, Calendar toDate) {
        validateToGetAccountHistory(accountId, fromDate, toDate);
        Account account = getAccount(accountId);
        return findTransactionsInRange(account, fromDate, toDate);
    }
    
    private Collection<Record> getAccountHistory(String accountId, Calendar fromDate, Calendar toDate) {
        validateToGetAccountHistory(accountId, fromDate, toDate);
        Account account = getAccount(accountId);
        return findAccountHistoryInRange(account, fromDate, toDate);
    }    
    
    /**
     * Finds transactions of the account between fromDate to toDate.
     * @return collection of transactions
     */
    public Collection<Record> findTransactionsInRange(Account account, Calendar fromDate, Calendar toDate) {
        Collection<Record> resultList = new TreeSet<>(new RecordHistoryComparator());
        Calendar date = Calendar.getInstance();
        date.setTime(fromDate.getTime());
        if(date.before(account.getAccountOpenDate())) {
            date.setTime(account.getAccountOpenDate().getTime());
        }
        if(date.after(toDate) && !DateUtils.isSameDay(date, toDate)) {
            return resultList;
        }
        while(!date.after(toDate) || DateUtils.isSameDay(date, toDate)) {
            TreeSet daysTransactions = account.getTransactionsFrom(date);
            if(daysTransactions != null) {
                resultList.addAll(daysTransactions);
            }
            date.add(Calendar.DATE, 1);
        }
        return resultList;
    }
    
    /**
     * Finds account history of the account between fromDate to toDate.
     * @return collection of account history
     */
    public Collection<Record> findAccountHistoryInRange(Account account, Calendar fromDate, Calendar toDate) {
        Collection<Record> accountHistories = new TreeSet<>(new RecordHistoryComparator());
        Calendar date = Calendar.getInstance();
        date.setTime(fromDate.getTime());
        if(date.before(account.getAccountOpenDate())) {
            date.setTime(account.getAccountOpenDate().getTime());
        }
        if(date.after(toDate) && !DateUtils.isSameDay(date, toDate)) {
            return accountHistories;
        }
        while(!date.after(toDate) || DateUtils.isSameDay(date, toDate)) {
            TreeSet daysAccountHistory = account.getAccountHistoryByDate(date);
            if(daysAccountHistory != null) {
                accountHistories.addAll(daysAccountHistory);
            }
            date.add(Calendar.DATE, 1);
        }
        return accountHistories;
    }
    
    public void addAccountHolder(String accountId, String customerId) {
        checkAccountExistance(accountId);
        checkCustomerExistance(customerId);
        if (customerController.getCustomerAccount(customerId, accountId) != null) {
            throw new InputValidationException("The customer already holds this account");
        }
        Customer customer = customerController.getCustomer(customerId);
        Account account = getAccount(accountId);
        customer.addAccount(account);
        account.addAccountHolder(customer);
        LOGGER.info("Successfully added account holder");
    }
    
    /**
     * Calculates interest and penalty payment.
     * The calculation occurs daily basis.
     * Visitor design pattern is used for the interest and penalty calculation.
     */
    public void calculateInterestOrPenalty() {
        InterestCalculationVisitor interest = new InterestCalculationVisitor();
        PenaltyCalculationVisitor penalty = new PenaltyCalculationVisitor();
        Collection<Account> accounts = accountStore.getAccounts().values();
        for(Account account: accounts) {
            account.accept(interest);
            account.accept(penalty);
        }
    }
    
    /**
     * Pays the calculated interest and penalty payment.
     * The payment occurs once at the end of the month.
     * Visitor design pattern is used for paying the interest and penalty payment.
     */
    public void payInterestAndPenalty() {
        InterestPaymentVisitor interest = new InterestPaymentVisitor();
        PenaltyPaymentVisitor penalty = new PenaltyPaymentVisitor();
        Collection<Account> accounts = accountStore.getAccounts().values();
        for(Account account: accounts) {
            account.accept(interest);
            account.accept(penalty);
        }
    }
    
    public Set<String> getAccountTypes() {
        return accountFactories.keySet();
    }

    public HashMap<String, AccountFactory> getAccountFactories() {
        return accountFactories;
    }

    public Collection<Account> getAccounts() {
        return accountStore.getAccounts().values();
    }
    
    public AccountFactory getAccountFactory(String loanType){
        return accountFactories.get(loanType);
    }
    
    protected void addAccount(Account account) {
        accountStore.add(account);
    } 
    
    public Account getAccount(String accountId) {
        validateAccountId(accountId);
        int id = Integer.parseInt(accountId);
        return accountStore.getAccount(id);
    }
}
