package com.msd.bank.controller;

import com.msd.bank.model.account.Account;
import java.util.HashMap;

/**
 * This class is to store all the created accounts.
 * Singleton design pattern is used on this class.
 */
public class AccountStore {
    
    private static final HashMap<Integer, Account> ACCOUNTS = new HashMap<>();
    private static final AccountStore SINGLETON = new AccountStore();
    
    private AccountStore(){}

    /**
     * @return singleton instance
     */
    public static AccountStore getInstance() {
        return SINGLETON;
    }
    
    public HashMap<Integer, Account> getAccounts() {
        return ACCOUNTS;
    }
    
    public Account getAccount(Integer accountId) {
        return ACCOUNTS.get(accountId);
    }
    
    public Account getAccount(String accountId) {
        return ACCOUNTS.get(Integer.parseInt(accountId));
    }
    
    public void add(Account account) {
        Integer accountId = account.getAccountId();
        ACCOUNTS.put(accountId, account);
    }
    
    public void clear() {
        ACCOUNTS.clear();
    }
}
