package com.msd.bank.controller;

import static com.msd.bank.util.InputValidationUtils.*;
import com.msd.bank.util.InputValidationException;
import com.msd.bank.model.account.base.*;
import com.msd.bank.model.account.Account;
import com.msd.bank.model.Customer;
import com.msd.bank.util.Logger;
import com.msd.bank.model.account.AccountFactory;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * The class is the controller of BaseAccount class.
 * It contains methods related to BaseAccount.
 * To ensure only one instance of the class is created, singleton design pattern is used.
 */
public class BaseAccountController extends AccountController {
    
    private static final Logger LOGGER = new Logger(BaseAccountController.class);
    private static final BaseAccountController SINGLETON = new BaseAccountController();
    private final CustomerController customerController = CustomerController.getInstance();
    private final AccountStore accountStore = AccountStore.getInstance();  
    
    private BaseAccountController() {
        registerAccountFactories();
    }

    /**
     * @return singleton instance
     */
    public static BaseAccountController getInstance() {
        return SINGLETON;
    }
    
    /**
     * Registers all the base account factories.
     */
    private void registerAccountFactories() {
        HashMap accountFactories = getAccountFactories();
        registerAccountFactory(accountFactories, new BusinessAccount.BusinessAccountFactory());
        registerAccountFactory(accountFactories, new ChildAccount.ChildAccountFactory());
        registerAccountFactory(accountFactories, new CurrentAccount.CurrentAccountFactory());
        registerAccountFactory(accountFactories, new DisabilityAccount.DisabilityAccountFactory());
        registerAccountFactory(accountFactories, new FeesInterestAccount.FeesInterestAccountFactory());
        registerAccountFactory(accountFactories, new HighInterestAccount.HighInterestAccountFactory());
        registerAccountFactory(accountFactories, new IRAccount.IRAccountFactory());
        registerAccountFactory(accountFactories, new InternationalAccount.InternationalAccountFactory());
        registerAccountFactory(accountFactories, new IslamicAccount.IslamicAccountFactory());
        registerAccountFactory(accountFactories, new LCRAccount.LCRAccountFactory());
        registerAccountFactory(accountFactories, new PrivateAccount.PrivateAccountFactory());
        registerAccountFactory(accountFactories, new SMBAccount.SMBAccountFactory());
        registerAccountFactory(accountFactories, new SavingsAccount.SavingsAccountFactory());
        registerAccountFactory(accountFactories, new StudentAccount.StudentAccountFactory());
        registerAccountFactory(accountFactories, new HolidayAccount.HolidayAccountFactory());
        registerAccountFactory(accountFactories, new RetirementAccount.RetirementAccountFactory());
        registerAccountFactory(accountFactories, new StandardAccount.StandardAccountFactory());
    }
    
    public void createAccount(String accType, String accountIdInput, String customerId) {
        checkAccountCreationInput(accType, accountIdInput, customerId, this);       
        int accountId = Integer.parseInt(accountIdInput);
        Customer owner = customerController.getCustomer(customerId);
        // Create account with account factory.
        AccountFactory accountFactory = getAccountFactories().get(accType);
        BasicAccountFactory basicAccountFactory = (BasicAccountFactory) accountFactory;
        BaseAccount account = basicAccountFactory.createBaseAccount(owner, accountId);
        // Associates account with the customer
        owner.addAccount(account);
        accountStore.add(account);           
        LOGGER.info("Your account ID is " + accountId);
    }
    
        /**
     * Checks whether all the field filled correctly, customer existence and account existence.
     * If account ID already exist, a user can not create an account.
     */
    private void checkAccountCreationInput(String accountType, String accountId, String customerId, AccountController controller) {
        validateAccountType(accountType, controller);
        validateCustomerId(customerId);
        checkCustomerExistance(customerId);
        validateAccountId(accountId);
        if(getAccount(accountId) != null) {
            throw new InputValidationException("Account ID " + accountId + " already exist.");
        }
    }

    public BigDecimal viewAccountBalance(String accountId) {
        validateAccountId(accountId);
        Account account = getAccount(accountId);
        return account.viewBalance();
    }

    public void withdrawMoney(String accountId, String amountInput) {
        validateAccountIdAndAmount(accountId, amountInput);
        Account account = getAccount(accountId);
        BigDecimal amount = new BigDecimal(amountInput);
        account.withdrawMoney(amount);
    }
    
    /**
     * @return the collection of the base accounts held by the specific customer
     */
    public Collection<BaseAccount> getAllBaseAccountsByCustomer(String customerId) {
        Customer customer = customerController.getCustomer(customerId);
        Collection<Account> accounts = customer.getAccounts();
        Collection<BaseAccount> baseAccounts = new ArrayList<>();
        for(Account account: accounts) {
            if (account instanceof BaseAccount) {
                baseAccounts.add((BaseAccount) account);
            }
        }
        return baseAccounts;
    }
    
    public void changeOverdraftLimit(String accountId, String inputLimitAmount) {
        validateAccountIdAndAmount(accountId, inputLimitAmount);
        Account account = getAccount(accountId);
        BigDecimal limitAmount = new BigDecimal(inputLimitAmount);
        account.changeOverdraftLimit(limitAmount);
        LOGGER.info("Overdraft limit successfully changed to " + limitAmount);
    }
    
    public BaseAccount getBaseAccount(String accountId) {
        BaseAccount account = (BaseAccount) getAccount(accountId);
        if(!(account instanceof BaseAccount)) {
            throw new InputValidationException("This account is not a type of Base Account");
        }
        return account;
    }
}
