package com.msd.bank.controller;

import static com.msd.bank.util.InputValidationUtils.*;
import com.msd.bank.util.InputValidationException;
import com.msd.bank.model.Customer;
import com.msd.bank.util.Logger;
import com.msd.bank.model.account.Account;
import java.util.HashMap;
import java.util.Set;

/**
 * The class is the controller of Customer class.
 * To ensure only one instance of the class is created, singleton design pattern is used.
 */
public class CustomerController {

    private static final Logger LOGGER = new Logger(CustomerController.class);
    private static final CustomerController SINGLETON = new CustomerController();
    private final HashMap<Integer, Customer> customers = new HashMap<>();
    
    private CustomerController() {};
    
    /**
     * @return singleton instance
     */
    public static CustomerController getInstance() {
        return SINGLETON;
    }
    
    public void createCustomer(String customerIdInput, String firstName, String lastName) {
        validateCustomerCreationInput(customerIdInput, firstName, lastName);
        int customerId = Integer.parseInt(customerIdInput);
        Customer customer = new Customer(customerId, firstName, lastName);
        customers.put(customerId, customer);
        LOGGER.info("Your customer ID is " + customerId);
    }
    
    /**
     * It throws InputValidationException if either invalid input or the customer ID is already used.
     */
    private void validateCustomerCreationInput(String customerId, String firstName, String lastName) {
        validateCustomerId(customerId);
        checkFilled("First Name", firstName);
        checkFilled("Last Name", lastName);
        if(getCustomer(customerId) != null) {
            throw new InputValidationException("Customer ID " + customerId + " already exist.");
        }
    }

    public Customer getCustomer(String customerId) {
        validateCustomerId(customerId);
        return customers.get(Integer.parseInt(customerId));
    }

    public Set<Integer> getCustomerIds() {
        return customers.keySet();
    }
    
   public Account getCustomerAccount(String customerId, String accountId) {
       return getCustomer(customerId).getAccount(accountId);
   }
   
   /**
    * Removes all the customers from the map.
    */
   public void clear(){
       customers.clear();
   }
}