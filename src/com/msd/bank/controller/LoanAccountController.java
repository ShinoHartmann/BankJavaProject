package com.msd.bank.controller;

import com.msd.bank.util.InputValidationException;
import com.msd.bank.model.account.loan.CarLoanAccount;
import com.msd.bank.model.account.loan.LoanAccount;
import com.msd.bank.model.account.Account;
import com.msd.bank.model.account.loan.LoanAccountFactory;
import com.msd.bank.model.Customer;
import com.msd.bank.model.account.loan.HomeLoanAccount;
import static com.msd.bank.util.InputValidationUtils.*;
import com.msd.bank.util.LoanCalculationUtils;
import com.msd.bank.util.Logger;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

/**
 * The class is the controller of LoanAccount class.
 * To ensure only one instance of the class is created, singleton design pattern is used.
 */
public class LoanAccountController extends AccountController{

    private static final Logger LOGGER = new Logger(LoanAccountController.class);
    private static final LoanAccountController SINGLETON = new LoanAccountController();
    private final CustomerController customerController = CustomerController.getInstance();
    
    private LoanAccountController(){
        registerAccountFactories();
    }
    
    /**
     * @return singleton instance
     */
    public static LoanAccountController getInstance() {
        return SINGLETON;
    }
    /**
     * Registers all the loan account factories.
     */
    private void registerAccountFactories() {
        HashMap accountFactories = getAccountFactories();
        registerAccountFactory(accountFactories, new HomeLoanAccount.HomeLoanAccountFactory());
        registerAccountFactory(accountFactories, new CarLoanAccount.CarLoanAccountFactory());
    }

    public void createLoanAccount(String loanType, String accountIdInput, String customerId, String loanAmountInput, String percentageInterestRate, String durationInMonthInput) {
        checkLoanAccountCreationInput(loanType, accountIdInput, customerId, loanAmountInput, percentageInterestRate, durationInMonthInput);
        int accountId = Integer.parseInt(accountIdInput);
        BigDecimal loanAmount = new BigDecimal(loanAmountInput);
        BigDecimal interestRate = new BigDecimal(percentageInterestRate).divide(new BigDecimal("100"));
        int durationInMonth = Integer.parseInt(durationInMonthInput);
        Customer owner = customerController.getCustomer(customerId);
        // creates a loan account with the loan account factory
        LoanAccountFactory loanAccountFactory = (LoanAccountFactory) getAccountFactory(loanType);
        LoanAccount account = loanAccountFactory.createLoanAccount(owner, loanType, accountId, durationInMonth, loanAmount, interestRate);
        // links customers to the account
        owner.addAccount(account);
        // add account to AccountStore
        addAccount(account);       
        LOGGER.info("Your loan account ID is " + accountId);
    }
    
    /**
     * This method validate input during account creation.
     * It throws InputValidationException if invalid input found.
     */
    private void checkLoanAccountCreationInput(String loanType, String accountId, String customerId, String loanAmount, String percentageInterestRate, String durationInMonth) {
        validateLoanAccountType(loanType, this);
        validateAccountId(accountId);
        validateCustomerId(customerId);
        checkCustomerExistance(customerId);
        validateBigDecimalInput("Amount",loanAmount);
        validateBigDecimalInput("Annual Interest Rate", percentageInterestRate);
        validateIntegerInput("Duration", durationInMonth);
        if(getAccount(accountId) != null) {
            throw new InputValidationException("Account ID " + accountId + " already exist.");
        }
        
    }
    
    public BigDecimal calculateMonthlyPayment(String amountInput, String interestRateInput, String durationInMonthInput) {
        validateBigDecimalInput("Amount", amountInput);
        validateBigDecimalInput("Annual Interest Rate", interestRateInput);
        validateIntegerInput("Duration", durationInMonthInput);
        BigDecimal amount = new BigDecimal(amountInput);
        BigDecimal interestRate = new BigDecimal(interestRateInput).divide(new BigDecimal(100), 10, RoundingMode.HALF_UP);
        int period = Integer.parseInt(durationInMonthInput);
        return LoanCalculationUtils.calculateMonthlyLoanPayment(amount, interestRate, period);
    }
    /**
     * This method is used when a customer would like to make a lump sum.
     */
    public void payLumpSum(String accountId, String loanAmountInput) {
        validateAccountIdAndAmount(accountId, loanAmountInput);
        LoanAccount account = getLoanAccount(accountId);
        BigDecimal loanAmount = new BigDecimal(loanAmountInput);
        BigDecimal remainingLoan = account.getRemainingLoan();
        // check whether loan amount exceeds the remaining loan
        if(loanAmount.compareTo(remainingLoan) > 0) {
            String message = "Remaining load of account ID " + accountId + " is £" 
                    + remainingLoan +". Please pay not more than £" + remainingLoan;
            throw new InputValidationException(message);
        } 
        account.depositMoney(loanAmount);
    }
    
    /**
     * This method is used when a customer would like to make a monthly payment.
     */
    public void payMonthlyLoan(String accountId) {
        LoanAccount account = (LoanAccount) getAccount(accountId);
        account.makeMontlyPayment();
    }

    public LoanAccount getLoanAccount(String accountId) {
        Account account = getAccount(accountId);
        if(!(account instanceof LoanAccount)) {
            throw new InputValidationException("Account ID " + accountId + " is not a Loan Account");
        }
        return (LoanAccount) account;
    }
}