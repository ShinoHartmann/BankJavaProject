package com.msd.bank.model;

import com.msd.bank.model.account.Account;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * It contains a customers data.
 */
public class Customer {
    
    private final int customerId;
    private final Map<Integer, Account> accounts = new HashMap<>();
    private final String firstName;
    private final String lastName;
    
    public Customer(int customerId, String firstName, String lastName) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    /**
     * Associates the customer with this account.
     */
    public void addAccount(Account account) {
        accounts.put(account.getAccountId(), account);
    }
    
    /**
     * Returns all the accounts which this customer holds.
     */
    public Collection<Account> getAccounts() {
        return accounts.values();
    }
    
    public Account getAccount(String accountId) {
        return accounts.get(Integer.parseInt(accountId));
    }
    
    public int getCustomerId() {
        return customerId;
    }
    
    public String getName() {
        return firstName + " " + lastName;
    }
}
