package com.msd.bank.model.account;

import static com.msd.bank.model.account.record.TransactionType.*;
import static com.msd.bank.model.account.record.AccountHistoryType.*;
import com.msd.bank.model.account.visitor.*;
import com.msd.bank.model.account.record.*;
import com.msd.bank.model.account.memento.Caretaker;
import com.msd.bank.model.account.memento.Memento;
import com.msd.bank.model.Customer;
import com.msd.bank.util.InputValidationException;
import com.msd.bank.util.TimeProvider;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 * Abstract class for all accounts.
 */
public abstract class Account implements Visitable, Cloneable {
   
    private final TimeProvider timeProvider = TimeProvider.getInstance();
    private final Map<Integer, Customer> accountHolders = new HashMap<>();
    private final Map<String, TreeSet<Transaction>> transactions = new HashMap<>();
    private final Map<String, TreeSet<AccountHistory>> accountHistory = new HashMap<>();
    private final Customer accountOwner;
    private final Caretaker caretaker = new Caretaker();
    private final BigDecimal interestRate;
    private final BigDecimal penaltyRate;
    private final int accountId;
    private final String accountType;    
    private final Calendar accountOpenDate;
    private Calendar lastInterestCalculationDate;
    private BigDecimal balance = new BigDecimal(0);
    private BigDecimal interestAmount = new BigDecimal(0);
    private BigDecimal penaltyAmount = new BigDecimal(0);

    protected Account(Customer accountOwner, String accountType, int accountId, BigDecimal interestRate, BigDecimal penaltyRate){
        this.accountId = accountId;
        this.accountType = accountType;
        this.interestRate = interestRate;
        this.penaltyRate = penaltyRate;
        this.accountOwner = accountOwner;
        accountHolders.put(accountOwner.getCustomerId(), accountOwner);
        accountOpenDate = timeProvider.getToday();
        lastInterestCalculationDate = timeProvider.getToday();
        addAccountHistory(OpenAccount);
    }

    public void addAccountHolder(Customer customer) {
        accountHolders.put(customer.getCustomerId(), customer);
        addAccountHistory(AddAccountHolder);
    }
    
    public void moneyIn(TransactionType transactionType,BigDecimal amount) {
        setBalance(amount);
        addTransaction(transactionType, amount);
    }
    
    public void moneyOut(TransactionType transactionType,BigDecimal amount) {
        setBalance(amount.negate());
        addTransaction(transactionType, amount);
    }
    
    public void withdrawMoney(BigDecimal amount) {
        if (!hasSufficientFunds(amount)) {
            throw new InputValidationException("Insufficient funds. This transaction has been cancelled.");
        }
    }
    
    public void transferOut(BigDecimal amount) {
        throw new InputValidationException("Unable to transfer money from this account (accountType: " + accountType + ")");
    }
    
    public void changeOverdraftLimit(BigDecimal limit) {
        throw new InputValidationException("This account doesn't support overdraft (accountType: " + accountType + ")");
    }
    
    public boolean hasSufficientFunds(BigDecimal amount) {
        return false;
    }
                 
    protected void setBalance(BigDecimal amount) {
        balance = balance.add(amount);
    }
    
    public BigDecimal getBalance() {
        return balance;
    }
    
    public BigDecimal getInterestRate() {
        return interestRate;
    }
    
    /**
     * Saves cloned account into caretaker.
     */
    public Memento saveToMemento(Record record) {
        return new Memento(createClone(), record);
    }

    public TreeSet<Transaction> getTransactionsFrom(Calendar calendarDate) {
        String date = getStringDate(calendarDate);
        return transactions.get(date);
    }
    
    public String getStringDate(Calendar dateTime) {
        DateFormat dateFormat =  new SimpleDateFormat("dd/M/yyyy");
        Date date = dateTime.getTime();
        return dateFormat.format(date);
    }
    
    /** 
     * When a customer makes an action related to the account balance, 
     * a new transaction is added to the transaction map.
     * 
     * The key of the transaction map is the creation date.
     * The value of the transaction map is a TreeSet which stores all the transactions of the same day.
     * 
     * @param transactionType is chosen from TransactionType enum class
     */
    public void addTransaction(TransactionType transactionType, BigDecimal amount) {
        Transaction transaction = new Transaction(this, transactionType, amount);
        String date = transaction.getRecordDateAsString();
        TreeSet<Transaction> transactionsOfDay = transactions.get(date);
        if(transactionsOfDay == null) {
            transactionsOfDay = new TreeSet<>(new RecordHistoryComparator());
            transactions.put(date, transactionsOfDay);
        }
        transactionsOfDay.add(transaction);
        caretaker.add(saveToMemento(transaction));
    }
    
    /** 
     * When a customer makes an action unrelated to the account balance, 
     * a new account history object is added to the account history map.
     * 
     * The key of the transaction map is the creation date.
     * The value of the transaction map is a TreeSet which stores all the transactions of the same day.
     * 
     * @param historyType is chosen from AccountHistoryType enum class.
     */
    protected final void addAccountHistory(AccountHistoryType historyType) {
        AccountHistory history = new AccountHistory(historyType);
        String date = history.getRecordDateAsString();
        TreeSet<AccountHistory> historiesOfDay = accountHistory.get(date);
        if(historiesOfDay == null) {
            historiesOfDay = new TreeSet<>(new RecordHistoryComparator());
            accountHistory.put(date, historiesOfDay);
        }
        historiesOfDay.add(history);
        caretaker.add(saveToMemento(history));
    }
    
     public TreeSet<AccountHistory> getAccountHistoryByDate(Calendar calendarDate) {
        String date = getStringDate(calendarDate);
        return accountHistory.get(date);
    }
    
    public String getAccountType() {
        return accountType;
    }

    public Map<Integer, Customer> getAccountHolders() {
        return accountHolders;
    }

    public Customer getAccountOwner() {
        return accountOwner;
    }

    public BigDecimal getPenaltyRate() {
        return penaltyRate;
    }
    
    public BigDecimal viewBalance() {
        addTransaction(ViewBalance, getBalance());
        return balance;
    }

    public Map<String, TreeSet<Transaction>> getAllTransactions() {
        return transactions;
    }

    public Map<String, TreeSet<AccountHistory>> getAllAccountHistories() {
        return accountHistory;
    }

    public Calendar getAccountOpenDate() {
        return accountOpenDate;
    }
    
    /**
     * This method is called when a customer likes to cancel a transaction.
     * The memento design pattern is used to version the account and rollback.
     * The last transaction will be deleted from the current transaction list.
     * 
     * @return the previous state of the account
     */
    public Account rollback() {
        Memento latestMemento = caretaker.getLatestMemento();
        Memento previousMemento = caretaker.getPreviousMemento();
        if(latestMemento.getRecord() instanceof Transaction) {
            Transaction lastTransaction = (Transaction) latestMemento.getRecord();
            Collection<Transaction> transactionList = transactions.get(lastTransaction.getRecordDateAsString());
            if(!transactionList.remove(lastTransaction)) {
                String message = "Rollback failed as transaction " + lastTransaction + " could not be removed from transaction list";
                throw new RuntimeException(message);
            }
        } else {
            AccountHistory lastAccountHistory = (AccountHistory) latestMemento.getRecord();
            Collection<AccountHistory> historyList = accountHistory.get(lastAccountHistory.getRecordDateAsString());
            if(!historyList.remove(lastAccountHistory)) {
                String message = "Rollback failed as account history " + lastAccountHistory + " could not be removed from history list";
                throw new RuntimeException(message);
            }
        }
        return previousMemento.getAccount(); 
    }
    
    public Account createClone() {
        try {
            return (Account) clone();
        } catch(CloneNotSupportedException ex) {
            throw new RuntimeException(ex.toString(), ex);
        }
    }
    
    public Customer getAccountHolder(String customerId) {
        return accountHolders.get(Integer.parseInt(customerId));
    }
    
    public String getOwnerName() {
        return accountOwner.getName();
    }
    
    public ArrayList<String> getAccountHolderNames() { 
        ArrayList<String> holderNames = new ArrayList<>();
        for(Customer customer: accountHolders.values()) {
            holderNames.add(customer.getName());
        }
        return holderNames;
    }

    public int getAccountId() {
        return accountId;
    }
    
    public Calendar getLastInterestCalculationDate() {
        return lastInterestCalculationDate;
    }

    public void setLastInterestCalculationDate(Calendar lastInterestPaymentDate) {
        this.lastInterestCalculationDate = lastInterestPaymentDate;
    }

    public BigDecimal getInterestAmount() {
        return interestAmount.setScale(3, BigDecimal.ROUND_HALF_UP);
    }

    public void resetInterestAmount() {
        interestAmount = new BigDecimal(0);
    }
    
    public void incrementInterestAmount(BigDecimal amount) {
        interestAmount = interestAmount.add(amount);
    }

    public BigDecimal getPenaltyAmount() {
        return penaltyAmount;
    }

    public void incrementPenaltyAmount(BigDecimal amount) {
        penaltyAmount = penaltyAmount.add(amount);
    }
    
    public void resetPenaltyAmount() {
        penaltyAmount =  new BigDecimal(0);
    }
    
    @Override
    public abstract void accept(Visitor visitor);
    public abstract void depositMoney(BigDecimal amount);
    public abstract void transferIn(BigDecimal amount);
}