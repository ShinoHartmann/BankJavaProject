package com.msd.bank.model.account;

/**
 * Base interface for all account factories.
 * 
 * It doesn't contain the createAccount method
 * because each account type needs different parameters.
 */
public interface AccountFactory {
    String getAccountType();
}
