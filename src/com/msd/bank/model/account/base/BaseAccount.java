package com.msd.bank.model.account.base;

import com.msd.bank.util.Logger;
import com.msd.bank.model.account.Account;
import com.msd.bank.model.Customer;
import com.msd.bank.model.account.visitor.Visitor;
import com.msd.bank.util.InputValidationException;
import java.math.BigDecimal;
import static com.msd.bank.model.account.record.AccountHistoryType.*;
import com.msd.bank.model.account.record.TransactionType;
import static com.msd.bank.model.account.record.TransactionType.*;

public abstract class BaseAccount extends Account {
    
    private static final Logger LOGGER = new Logger(BaseAccount.class);
    private final static BigDecimal PENALTY_RATE = new BigDecimal("0.15");
    private final int withdrawLimit;
    private BigDecimal overdraftLimit;
    private BigDecimal overdraft = BigDecimal.ZERO;

    protected BaseAccount(Customer accountOwner, String accountType, int accountId, int withdrawLimit, 
            BigDecimal interestRate, int overdraftLimit){
        super(accountOwner, accountType, accountId, interestRate, PENALTY_RATE);
        this.withdrawLimit = withdrawLimit;
        this.overdraftLimit = new BigDecimal(overdraftLimit);
    }

    public int getWithdrawLimit() {
        return withdrawLimit;
    }
    
    public BigDecimal getOverdraft() {
        return overdraft;
    }
    
    public void payOverdraft(BigDecimal amount) {
        overdraft = overdraft.subtract(amount);
        if(overdraft.compareTo(BigDecimal.ZERO) < 0) {
            overdraft = BigDecimal.ZERO;
        }
    }

    @Override
    public void setBalance(BigDecimal amount) {
        super.setBalance(amount);
        if(getBalance().compareTo(BigDecimal.ZERO) < 0) {
            setOverdraft(getBalance());
        }
    }    
    
    private void setOverdraft(BigDecimal amount) {
        if(this.overdraft.add(amount).compareTo(overdraftLimit) > 0) {
            throw new InputValidationException("Transaction has been canceled. You exceed the overdraft limit of " + overdraftLimit + " on this account.");
        }
        overdraft = amount;
    }
    
    public void setOverdraftLimit(BigDecimal overdraftLimit) {
        this.overdraftLimit = overdraftLimit;
    }
    
    public BigDecimal getOverdraftLimit() {
        return overdraftLimit;
    }

    public BigDecimal getAvailableBalance() {
        return getBalance().add(overdraftLimit);
    }
    
    @Override
    public void changeOverdraftLimit(BigDecimal limit) {
        overdraftLimit = limit;
        addAccountHistory(OverdraftLimitSet);
    }
        
    @Override
    public void moneyOut(TransactionType transactionType,BigDecimal amount) {
        super.moneyOut(transactionType, amount);
        if(getBalance().compareTo(BigDecimal.ZERO) < 0) {
            BigDecimal overdraftAmount = getBalance().negate();
            setOverdraft(overdraftAmount);
        }
    }
    
    @Override
    public void withdrawMoney(BigDecimal amount) {
        super.withdrawMoney(amount);
        if (exceedsWithdrawLimit(amount)) {
            throw new InputValidationException("The maximum withdrawal for a " + getAccountType()
                    + " is £" + withdrawLimit
                    + ". This transaction has been cancelled.");
        }
        moneyOut(Withdraw, amount);
        LOGGER.info("Withdrawal of £" + amount + " has been made successfully");
    }
    
    @Override
    public void depositMoney(BigDecimal amount) {
        moneyIn(Deposit, amount);
        LOGGER.info("Deposit of £" + amount + " has been made successfully"); 
    }
    
    @Override
    public void transferIn(BigDecimal amount) {
        moneyIn(TransferIn, amount);
    }
    
    @Override
    public void transferOut(BigDecimal amount) {
        moneyOut(TransferOut, amount);
    }
    
    @Override
    public boolean hasSufficientFunds(BigDecimal amount) {
        return getAvailableBalance().compareTo(amount) >= 0;
    }
    
     public boolean exceedsWithdrawLimit(BigDecimal amount) {
        return amount.compareTo(new BigDecimal(getWithdrawLimit())) > 0;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}