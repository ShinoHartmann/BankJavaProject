package com.msd.bank.model.account.base;

import com.msd.bank.model.account.AccountFactory;
import com.msd.bank.model.Customer;

/**
 * Base interface for all base account factory implementations.
 */
public interface BasicAccountFactory extends AccountFactory {
    BaseAccount createBaseAccount(Customer owner, int accountId);
}
