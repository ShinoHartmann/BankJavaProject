package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class BusinessAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "Business Account";
    private static final int WITHDRAW_LIMIT = 2000;
    private static final BigDecimal INTEREST_RATE = new BigDecimal("0.02");
    private static final int OVERDRAFT_LIMIT = 5000;    
    
    public BusinessAccount(Customer businessName, int accountId) {
        super(businessName, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }

    public static class BusinessAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new BusinessAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}