package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class ChildAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "Child Account";
    private static final int WITHDRAW_LIMIT = 200;
    private static final BigDecimal INTEREST_RATE = new BigDecimal(0);
    private static final int OVERDRAFT_LIMIT = 0;
    
    public ChildAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }
    
    public static class ChildAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new ChildAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
