package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class FeesInterestAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "Fee Interest Account";
    private static final int WITHDRAW_LIMIT = 0;
    private static final BigDecimal INTEREST_RATE = new BigDecimal(0);
    private static final int OVERDRAFT_LIMIT = 0;

    public FeesInterestAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId,WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }
    public static class FeesInterestAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new FeesInterestAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
