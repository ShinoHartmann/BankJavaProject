package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class HighInterestAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "High Interest Account";
    private static final int WITHDRAW_LIMIT = 1000;
    private static final BigDecimal INTEREST_RATE = new BigDecimal("0.08");
    private static final int OVERDRAFT_LIMIT = 500;

    public HighInterestAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }

     public static class HighInterestAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new HighInterestAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
