package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class HolidayAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "Holiday Account";
    private static final int WITHDRAW_LIMIT = 2000;
    private static final BigDecimal INTEREST_RATE = new BigDecimal("0.05");
    private static final int OVERDRAFT_LIMIT = 5000;    
    
    public HolidayAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }

    public static class HolidayAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new HolidayAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
