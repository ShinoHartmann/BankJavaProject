package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class IRAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "IRA Account";
    private static final int WITHDRAW_LIMIT = 200;
    private static final BigDecimal INTEREST_RATE = new BigDecimal("0.02");
    private static final int OVERDRAFT_LIMIT = 500;
    
    public IRAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }
    
    public static class IRAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new IRAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
