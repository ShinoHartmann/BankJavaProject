package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class IslamicAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "Islamic Account";
    private static final int WITHDRAW_LIMIT = 500;
    private static final BigDecimal INTEREST_RATE = new BigDecimal("0.04");
    private static final int OVERDRAFT_LIMIT = 1000;
    
    public IslamicAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }
    public static class IslamicAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new IslamicAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
