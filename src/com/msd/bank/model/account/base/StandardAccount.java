package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class StandardAccount extends BaseAccount{
    private static final String ACCOUNT_TYPE = "Standard Account";
    private static final int WITHDRAW_LIMIT = 500;
    private static final BigDecimal INTEREST_RATE = new BigDecimal("0.03");
    private static final int OVERDRAFT_LIMIT = 1000;    
    
    public StandardAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }

    public static class StandardAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new StandardAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
