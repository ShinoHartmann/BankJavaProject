package com.msd.bank.model.account.base;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class StudentAccount extends BaseAccount {
    
    private static final String ACCOUNT_TYPE = "Student Account";
    private static final int WITHDRAW_LIMIT = 300;
    private static final BigDecimal INTEREST_RATE = new BigDecimal("0.01");
    private static final int OVERDRAFT_LIMIT = 2000;

    public StudentAccount(Customer owner, int accountId) {
        super(owner, ACCOUNT_TYPE, accountId, WITHDRAW_LIMIT, INTEREST_RATE, OVERDRAFT_LIMIT);
    }

    public static class StudentAccountFactory implements BasicAccountFactory {
        @Override
        public BaseAccount createBaseAccount(Customer owner, int accountId) {
            return new StudentAccount(owner, accountId);
        }

        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
