package com.msd.bank.model.account.loan;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class CarLoanAccount extends LoanAccount{
    
    private static final String ACCOUNT_TYPE = "Car Loan Account";
    private final static BigDecimal LOAN_LIMIT = new BigDecimal(15000);
    
    public CarLoanAccount(Customer owner, String accountType, 
            int accountId, int durationInMonth,
            BigDecimal loanAmount, BigDecimal interestRate) {
        super(owner, accountType, accountId, durationInMonth, loanAmount, 
                interestRate, LOAN_LIMIT);
    }
    
    public static class CarLoanAccountFactory implements LoanAccountFactory {

        @Override
        public LoanAccount createLoanAccount(Customer owner, String accountType, 
                int accountId, int durationInMonth, 
                BigDecimal loanAmount, BigDecimal interestRate) {
            return new CarLoanAccount(owner, accountType, accountId,
                    durationInMonth, loanAmount, interestRate);
        }
        
        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}