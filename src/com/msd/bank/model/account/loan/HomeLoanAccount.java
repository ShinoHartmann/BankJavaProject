package com.msd.bank.model.account.loan;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;

public class HomeLoanAccount extends LoanAccount{
    
    private static final String ACCOUNT_TYPE = "Home Loan Account";
    private final static BigDecimal LOAN_LIMIT = null;
    
    public HomeLoanAccount(Customer owner, String accountType, 
            int accountId, int durationInMonth,
            BigDecimal loanAmount, BigDecimal interestRate) {
        super(owner, accountType, accountId, durationInMonth, loanAmount, 
                interestRate, LOAN_LIMIT);
    }
    
    public static class HomeLoanAccountFactory implements LoanAccountFactory {
        
        @Override
        public LoanAccount createLoanAccount(Customer owner, String accountType, 
                int accountId, int durationInMonth, 
                BigDecimal loanAmount, BigDecimal interestRate) {
            return new HomeLoanAccount(owner, accountType, accountId,
                    durationInMonth, loanAmount, interestRate);
        }
        
        @Override
        public String getAccountType() {
            return ACCOUNT_TYPE;
        }
    }
}
