package com.msd.bank.model.account.loan;

import static com.msd.bank.util.LoanCalculationUtils.*;
import static com.msd.bank.model.account.record.TransactionType.*;
import com.msd.bank.util.Logger;
import com.msd.bank.model.account.Account;
import com.msd.bank.model.Customer;
import com.msd.bank.model.account.visitor.Visitor;
import com.msd.bank.util.TimeProvider;
import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Abstract loan account which contains all basic loan account routines.
 */
public abstract class LoanAccount extends Account {
    
    private static final Logger LOGGER = new Logger(LoanAccount.class);
    private static final BigDecimal PENALTY_RATE = new BigDecimal("0.2");
    private final TimeProvider timeProvider = TimeProvider.getInstance();
    private int durationInMonth;
    private BigDecimal monthlyPaymentAmount;
    private BigDecimal loanAmount;
    private Calendar lastPaymentDate;
    
    public LoanAccount(Customer owner, String accountType, int accountId, int durationInMonth, 
            BigDecimal loanAmount, BigDecimal interestRate, BigDecimal loanLimit) {
        super(owner, accountType, accountId, interestRate, PENALTY_RATE);
        if(loanLimit != null && loanAmount.compareTo(loanLimit) > 0) {
            throw new IllegalArgumentException("The loan amount limit of the account is: " 
                    + loanLimit + ". Please reduce the loan amount.");
        }
        this.durationInMonth = durationInMonth;
        this.loanAmount = loanAmount;
        setBalance(loanAmount.negate());
        monthlyPaymentAmount = calculateMonthlyLoanPayment(loanAmount, interestRate, durationInMonth);
        lastPaymentDate = timeProvider.getToday();
    }
    
    /**
     * When a customer deposits money into this account, the money is treated as a loan payment.
     */
    @Override
    public void depositMoney(BigDecimal amount) {
        moneyIn(LoanPayment, amount);
        LOGGER.info("You have £" + getRemainingLoan() + " of your loan still to pay off");
    }
    
    @Override
    public void transferIn(BigDecimal amount) {
        depositMoney(amount);
    }

    @Override
    public final void setBalance(BigDecimal amount) {
        super.setBalance(amount);
        // Everytime the balance is changed, the monthly payment should be re-calculated.
        monthlyPaymentAmount = calculateMonthlyLoanPayment(getRemainingLoan(), getInterestRate(), durationInMonth);
    }
    
    public void makeMontlyPayment() {
        BigDecimal interestFee = calculateMonthlyInterest(getRemainingLoan(), getInterestRate());
        // This interest fee must be paid to the bank
        BigDecimal amount = monthlyPaymentAmount.subtract(interestFee);
        moneyIn(MonthlyPayment, amount);
        lastPaymentDate = timeProvider.getToday();
        durationInMonth--;
    }
    
    public int getdurationInMonth() {
        return durationInMonth;
    }

    public BigDecimal getMonthlyPaymentAmount() {
        return monthlyPaymentAmount;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public Calendar getLastPaymentDate() {
        return lastPaymentDate;
    }    
    
    public BigDecimal getRemainingLoan() {
        return getBalance().negate();
    }
    
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
