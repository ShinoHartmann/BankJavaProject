package com.msd.bank.model.account.loan;

import com.msd.bank.model.Customer;
import java.math.BigDecimal;
import com.msd.bank.model.account.AccountFactory;

/**
 * Base interface for all loan account factory implementations.
 */
public interface LoanAccountFactory extends AccountFactory {
    LoanAccount createLoanAccount(
            Customer owner, 
            String accountType, 
            int accountId, 
            int durationInMonth, 
            BigDecimal loanAmount, 
            BigDecimal interestRate
    );
}
