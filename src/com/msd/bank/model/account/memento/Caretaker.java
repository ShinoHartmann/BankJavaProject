package com.msd.bank.model.account.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * To rollback the state of an account, the memento design pattern is used.
 * The caretaker stores all states of an account (memento).
 */
public class Caretaker {
    
    private final List<Memento> mementoList = new ArrayList<>();

    public void add(Memento memento) {
        mementoList.add(memento);
    }
    
    public Memento get(int index) {
       return mementoList.get(index);
    }

    public Memento getLatestMemento() {
        if(mementoList.isEmpty()) {
            return null;
        }
        int index = mementoList.size() - 1;
        return mementoList.get(index);
    }
    
    public Memento getPreviousMemento() {
        if(mementoList.size() <= 1) {
            return null;
        }
        int index = mementoList.size() - 2;
        return mementoList.get(index);
    }
}
