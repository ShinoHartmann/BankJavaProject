package com.msd.bank.model.account.memento;

import com.msd.bank.model.account.Account;
import com.msd.bank.model.account.record.Record;

/**
 * Represents a state of an account at a given point in time.
 */
public class Memento {
    
    private final Account account;
    private final Record record;
    
    public Memento(Account account, Record record) {
        this.account = account;
        this.record = record;
    }

    public Account getAccount() {
        return account;
    }

    public Record getRecord() {
        return record;
    }
}
