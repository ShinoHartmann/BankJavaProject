package com.msd.bank.model.account.record;

import java.math.BigDecimal;

/**
 * Contains the account history other than amount transactions of an account.
 */
public class AccountHistory extends Record {
    
    private final AccountHistoryType accountHistoryType;

    public AccountHistory(AccountHistoryType accountHistoryType) {
        this.accountHistoryType = accountHistoryType;
    }
    
    @Override
    public String getType() {
        return accountHistoryType.toString();
    }

    @Override
    public BigDecimal getAmount() {
        return null;
    }

    @Override
    public BigDecimal getBalance() {
        return null;
    }
}
