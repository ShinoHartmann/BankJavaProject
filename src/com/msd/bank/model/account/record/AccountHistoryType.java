package com.msd.bank.model.account.record;

import com.msd.bank.model.account.Account;
import com.msd.bank.model.account.base.BaseAccount;

/**
 * This enumeration contains all the account history types.
 */
public enum AccountHistoryType {
    OpenAccount("Open New Account", Account.class),
    OverdraftLimitSet("New Overdraft Limit Set", BaseAccount.class),
    AddAccountHolder("Add New Account Holder", Account.class);
    
    private final String accountHistoryType;
    private final Class belongsClass;
    
    /**
     * @param accountHistoryType is the name of the account history.
     * @param belongsClass is where it is used.
     */
    private AccountHistoryType(String accountHistoryType, Class belongsClass) {
        this.accountHistoryType = accountHistoryType;
        this.belongsClass = belongsClass;
    }

    public Class getBelongsClass() {
        return belongsClass;
    }
        
    @Override
    public String toString() {
        return this.accountHistoryType;
    }
}
