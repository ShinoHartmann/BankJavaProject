package com.msd.bank.model.account.record;

import com.msd.bank.util.TimeProvider;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Base class for all account modification record types.
 */
public abstract class Record {
    
    private final TimeProvider timeProvider = TimeProvider.getInstance();
    private final Calendar recordDate;
    
    public Record() {
        recordDate = timeProvider.getToday();
    }
    
    public Calendar getRecordDate() {
        return recordDate;
    }
    
    public String getRecordDateAsString() {
        DateFormat dateFormat =  new SimpleDateFormat("dd/M/yyyy");
        Date date = recordDate.getTime();
        return dateFormat.format(date);
    }
    
    public abstract String getType();
    public abstract BigDecimal getAmount();
    public abstract BigDecimal getBalance();
}
