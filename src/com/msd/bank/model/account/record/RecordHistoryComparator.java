package com.msd.bank.model.account.record;

import java.util.Calendar;
import java.util.Comparator;
import org.apache.commons.lang.time.DateUtils;

/**
 * This comparator is used to order account modification records.
 * 
 * Records are ordered by date. If the account actions occur at the same time, 
 * the already contained list item has higher priority.
 */
public class RecordHistoryComparator implements Comparator<Record>{

    @Override
    public int compare(Record stateOne, Record stateTwo) {
        if(stateOne == stateTwo) {
            return 0;
        }
        
        Calendar dateOne = stateOne.getRecordDate();
        Calendar dateTwo = stateTwo.getRecordDate();
        
        if (dateOne.after(dateTwo)) {
            return 1;
        } else if (DateUtils.isSameDay(dateOne, dateTwo)) {
            return -1;
        } else {
            return -2;
        }
    }
}
