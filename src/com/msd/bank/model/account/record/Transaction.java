package com.msd.bank.model.account.record;

import com.msd.bank.model.account.Account;
import java.math.BigDecimal;

/**
 * Represents an account action which changed the account amount.
 */
public class Transaction extends Record {
    
    private final TransactionType transactionType;
    private final BigDecimal amount;
    private final BigDecimal balance;

    public Transaction(Account account, TransactionType transactionType, BigDecimal amount) {
        this.transactionType = transactionType;
        this.amount = amount;
        balance = account.getBalance();
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String getType() {
        return transactionType.toString();
    }    
}
