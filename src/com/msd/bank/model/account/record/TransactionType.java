package com.msd.bank.model.account.record;

import com.msd.bank.model.account.Account;
import com.msd.bank.model.account.base.BaseAccount;
import com.msd.bank.model.account.loan.LoanAccount;

public enum TransactionType {
    
    Deposit("Deposit", BaseAccount.class),
    Withdraw("Withdraw", BaseAccount.class),
    TransferIn("Transfer In", Account.class),
    TransferOut("Transfer Out", BaseAccount.class),
    LoanPayment("Loan Payment", LoanAccount.class),
    MonthlyPayment("Monthly Payment", LoanAccount.class),
    ViewBalance("View Balance", BaseAccount.class),
    InterestPaid("Interest Paid", BaseAccount.class),
    PenaltyPaid("Penalty Paid", BaseAccount.class);
    
    private final String transactionType;
    private final Class belongsClass;
    
    /**
     * @param belongsClass is used at ViewAccountRecordForm.
     */
    private TransactionType(String transactionType, Class belongsClass) {
        this.transactionType = transactionType;
        this.belongsClass = belongsClass;
    }

    public Class getBelongsClass() {
        return belongsClass;
    }
    
    @Override
    public String toString() {
        return this.transactionType;
    }
}
