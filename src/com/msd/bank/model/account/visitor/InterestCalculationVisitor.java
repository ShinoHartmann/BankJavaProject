package com.msd.bank.model.account.visitor;

import com.msd.bank.model.account.base.BaseAccount;
import com.msd.bank.model.account.loan.LoanAccount;
import com.msd.bank.util.TimeProvider;
import java.math.BigDecimal;
import java.util.Calendar;
import org.apache.commons.lang.time.DateUtils;

/**
 * Interest fee is calculated once a day and the calculated interest is accumulated and not added to the balance immediately.
 * This accumulated amount is payed once a month and it occurs at InterestPaymentVisitor class.
 */
public class InterestCalculationVisitor implements Visitor {
    
    private final TimeProvider timeProvider = TimeProvider.getInstance();
    
    /**
     * Calculates interest of the base account. 
     * If the current balance of this account is more than 0 then it calculates the interest.
     */
    @Override
    public void visit(BaseAccount baseAccount) {
        Calendar today = timeProvider.getToday();
        Calendar lastInterestPaymentDate = baseAccount.getLastInterestCalculationDate();
        // Checks whether this app already has calculated the interest today or not. 
        if(DateUtils.isSameDay(lastInterestPaymentDate, today)) {
            return;
        }
        BigDecimal balance = baseAccount.getBalance();
        BigDecimal interestRate = baseAccount.getInterestRate();
        // Interest should be calculated when the balance is more than 0.
        if (balance.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal yearlyInterestAmount = interestRate.multiply(balance);
            BigDecimal dailyInterstAmount = yearlyInterestAmount.divide(new BigDecimal(365), 10, BigDecimal.ROUND_HALF_UP);
            baseAccount.incrementInterestAmount(dailyInterstAmount);
            baseAccount.setLastInterestCalculationDate(today);
        } 
    }

    @Override
    public void visit(LoanAccount loanAccount) {
    //No interest needs to be calculated for loan accounts. Therefore this method is left empty
    }   
}
