package com.msd.bank.model.account.visitor;

import com.msd.bank.model.account.base.BaseAccount;
import com.msd.bank.model.account.loan.LoanAccount;
import java.math.BigDecimal;
import java.math.RoundingMode;
import static com.msd.bank.model.account.record.TransactionType.InterestPaid;

/**
 * Interest payment occur once a month. 
 */
public class InterestPaymentVisitor implements Visitor {

    /**
     * Put the interest amount for a basic account.
     */
    @Override
    public void visit(BaseAccount baseAccount) {
        BigDecimal interestAmount = baseAccount.getInterestAmount();
        if(interestAmount.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal amount = baseAccount.getInterestAmount();
            amount = amount.setScale(2, RoundingMode.HALF_UP);
            baseAccount.moneyIn(InterestPaid, amount);
            baseAccount.resetInterestAmount();
        }
    }

    @Override
    public void visit(LoanAccount loanAccount) {
        // Nothing has to be calculated for loan accounts 
        // as they don't receive interest from the bank
    }
    
}
