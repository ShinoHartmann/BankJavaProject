package com.msd.bank.model.account.visitor;

import com.msd.bank.model.account.base.BaseAccount;
import com.msd.bank.model.account.loan.LoanAccount;
import com.msd.bank.util.TimeProvider;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import org.apache.commons.lang.time.DateUtils;

public class PenaltyCalculationVisitor implements Visitor {
    
    private TimeProvider timeProvider = TimeProvider.getInstance();
    
    /**
     * Calculate penalty for a base account. 
     * If the current balance of this account is more than 0 then it calculates the interest.
     */
    @Override
    public void visit(BaseAccount baseAccount) {        
        Calendar today = timeProvider.getToday();
        // Checks whether this app already has calculated the penalty today or not. 
        // If the interst of the account has calculated today, exit this method.
        if(DateUtils.isSameDay(baseAccount.getLastInterestCalculationDate(), today)) {
           return; 
        }
        BigDecimal overdraft = baseAccount.getOverdraft();
        BigDecimal penaltyRate = baseAccount.getPenaltyRate().divide(new BigDecimal(12), 10, RoundingMode.HALF_UP);
        // Penalty fee should be calculated when the overdraft amount is more than 0.
        if (overdraft.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal penaltyFee = overdraft.multiply(penaltyRate);
            penaltyFee = penaltyFee.setScale(2, RoundingMode.HALF_UP);
            baseAccount.incrementPenaltyAmount(penaltyFee);
            baseAccount.setLastInterestCalculationDate(today);
        }
    }

    @Override
    public void visit(LoanAccount loanAccount) {
        // Nothing has to be calculated for loan accounts 
        // as they don't have overdraft limits
    }
    
}
