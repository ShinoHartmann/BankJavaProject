package com.msd.bank.model.account.visitor;

import com.msd.bank.util.LoanCalculationUtils;
import com.msd.bank.model.account.base.BaseAccount;
import com.msd.bank.model.account.loan.LoanAccount;
import com.msd.bank.util.Logger;
import com.msd.bank.util.TimeProvider;
import java.math.BigDecimal;
import java.util.Calendar;
import static com.msd.bank.model.account.record.TransactionType.PenaltyPaid;

/**
 * Adds the calculated penalty to an account.
 */
public class PenaltyPaymentVisitor implements Visitor{
    
    private final TimeProvider timeProvider = TimeProvider.getInstance();
    private static final Logger LOGGER = new Logger(PenaltyPaymentVisitor.class);
    
    /**
     * Adds a penalty to the account when the overdraft limit is used 
     * and the penaltyFee amount is more than 0.
     */
    @Override
    public void visit(BaseAccount baseAccount) {
        BigDecimal penaltyFee = baseAccount.getPenaltyAmount();
        // Check if penalty fee ever calculated in this month.
        if(penaltyFee.compareTo(BigDecimal.ZERO) > 0) {
            LOGGER.debug("Pay penalty for account: " + baseAccount.getAccountId());
            BigDecimal amount = baseAccount.getPenaltyAmount();
            baseAccount.moneyOut(PenaltyPaid, amount);
            // Reset penalty amount to zero for next month.
            baseAccount.resetPenaltyAmount();
        }
    }

    /**
     * Adds a penalty if a customer has not made a payment in this month.
     */
    @Override
    public void visit(LoanAccount loanAccount) {
        int lastPaymentMonth = loanAccount.getLastPaymentDate().get(Calendar.MONTH);
        int currentMonth = timeProvider.getToday().get(Calendar.MONTH);
        // Check if user made a payment this month
        if(lastPaymentMonth != currentMonth) {
            LOGGER.debug("Penalty calculated on acount: " + loanAccount.getAccountId());
            BigDecimal penaltyAmount = LoanCalculationUtils.calculatePenaltyAmount(loanAccount);
            loanAccount.moneyOut(PenaltyPaid, penaltyAmount);
        }
    }
}
