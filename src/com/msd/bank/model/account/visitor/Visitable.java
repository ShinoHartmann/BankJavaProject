package com.msd.bank.model.account.visitor;

/**
 * Visitable interface from the Visitor pattern.
 */
public interface Visitable {
    void accept(Visitor visitor);
}
