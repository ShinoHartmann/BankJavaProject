package com.msd.bank.model.account.visitor;

import com.msd.bank.model.account.base.BaseAccount;
import com.msd.bank.model.account.loan.LoanAccount;

/**
 * Visitor interface from the Visitor pattern.
 */
public interface Visitor {
    void visit(BaseAccount baseAccount);
    void visit(LoanAccount loanAccount);
}
