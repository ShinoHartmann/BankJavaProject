package com.msd.bank.scheduler;

import com.msd.bank.controller.BaseAccountController;
import com.msd.bank.util.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Initiates interest and penalty payment.
 */
public class InterestAndPenaltyPaymentJob implements Job {

    private static final Logger LOGGER = new Logger(InterestAndPenaltyCalculationJob.class);
    private final BaseAccountController controller = BaseAccountController.getInstance();

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.debug("Execute scheduled interest and penalty payment");
        controller.payInterestAndPenalty();
    }
}
