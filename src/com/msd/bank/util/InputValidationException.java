package com.msd.bank.util;

/**
 * Custom exception to throw validation errors.
 * If a user input incorrect information in the form, this exception is thrown.
 * This exception is handled in each form.
 */
public class InputValidationException extends RuntimeException {
    public InputValidationException(String message) {
        super(message);
    }
}
