package com.msd.bank.util;

import com.msd.bank.controller.AccountController;
import com.msd.bank.controller.AccountStore;
import com.msd.bank.controller.CustomerController;
import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Contains utility methods for validating user input.
 */
public final class InputValidationUtils {
    
    private static final AccountStore ACCOUNT_STORE = AccountStore.getInstance();
    private static final CustomerController CUSTOMER_CONTROLLER = CustomerController.getInstance();
    
    /**
     * Utility class shouldn't be instantiated.
     */
    private InputValidationUtils() {}

    public static void checkFilled(String fieldName, String input)  {
        if(input == null  || input.trim().equals("")){
            throw new InputValidationException("No value provided for " + fieldName);
        }
    }
    
    public static void checkFilled(String fieldName, Calendar input) {
        if(input == null){
            throw new InputValidationException("No value provided for " + fieldName);
        }
    }
    
    public static void validateIntegerValue(String fieldName, String input) {
        validateIntegerValue(fieldName, input, fieldName + " must be integer number");
    }
    
    public static void validateIntegerValue(String fieldName, String input, String message) {
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException ex) {
            throw new InputValidationException(message);
        }
    }
        
    public static void validatePositiveNumber(String fieldName, String input) {
        BigDecimal amount = new BigDecimal(input);
        if(!(amount.compareTo(BigDecimal.ZERO) > 0)) {
            throw new InputValidationException(fieldName + " must be more than 0");
        }
    }
    
    public static void validateBigDecimalValue(String fieldName, String input) {
        try {
            new BigDecimal(input);
        } catch (NumberFormatException ex) {
            throw new InputValidationException("input of " + fieldName + " must be decimal number");
        }
    }
    
    /**
     * FromDate must be before toDate. This method checks whether user correctly choose the date range.
     */
    public static void validateTwoDate(Calendar fromDate, Calendar toDate) {
        if(toDate.before(fromDate)) {
            throw new InputValidationException("From date must be before to date");
        }
    }

    public static void validateAccountType(String accountType, AccountController controller) {
        if(controller.getAccountFactories().get(accountType) == null) {
            throw new InputValidationException("Please select an account type");
        } 
    }
    
    public static void validateLoanAccountType(String accountType, AccountController controller) {
        if(controller.getAccountFactories().get(accountType) == null) {
            throw new InputValidationException("Please select a loan account type");
        } 
    }
    
    public static void checkCustomerExistance(String customerId) {
        validateCustomerId(customerId);
        if(CUSTOMER_CONTROLLER.getCustomer(customerId) == null) {
            throw new InputValidationException("This customer does not exist");
        }
    }
    
    public static void checkAccountExistance(String accountId) {
        validateAccountId(accountId);
        if(ACCOUNT_STORE.getAccount(accountId) == null) {
            throw new InputValidationException("This account does not exist");
        }
    }
    
    public static void validateCustomerId(String customerId) {
        checkFilled("Customer ID", customerId);
        validateIntegerValue("Customer ID", customerId, "Please select a Customer ID");
    }    
    
    public static void validateBigDecimalInput(String fieldName, String input) {
        checkFilled(fieldName, input);
        validateBigDecimalValue(fieldName, input);
        validatePositiveNumber(fieldName, input);
    }
    
    public static void validateIntegerInput(String fieldName, String input) {
        checkFilled(fieldName, input);
        validateIntegerValue(fieldName, input);
        validatePositiveNumber(fieldName, input);
    }
    
    public static void validateAccountId(String accountId) {
        checkFilled("Account ID", accountId);
        validateIntegerValue("Account ID", accountId, "Please select an Account ID");
    }
    
    public static void validateAccountIdAndAmount(String accountId, String amount) {
        checkAccountExistance(accountId);
        validateBigDecimalInput("Amount", amount);
    }
    
    public static void validateAccountIdAndAmount(String fromAccountNr, String toAccountNr, String amount) {        
        checkAccountExistance(fromAccountNr);
        checkAccountExistance(toAccountNr);
        if(fromAccountNr.equals(toAccountNr)) {
            throw new InputValidationException("You have chosen the same account. Please select the difference account");
        }
        validateBigDecimalInput("Amount", amount);
    }
    
    /**
     * Checks all the fields required to view account history.
     */
    public static void validateToGetAccountHistory(String accountId, Calendar fromDate, Calendar toDate) {
        checkFilled("From Date", fromDate);
        checkFilled("To Date", toDate);
        validateTwoDate(fromDate, toDate);
        validateAccountId(accountId);
        checkAccountExistance(accountId);
    }
}
