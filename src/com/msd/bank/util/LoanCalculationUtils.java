package com.msd.bank.util;

import com.msd.bank.model.account.loan.LoanAccount;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * This utility class contains methods for calculating the monthly payment and interest rate.
 */
public final class LoanCalculationUtils {
    
    /**
     * Utility class shouldn't be instantiated.
     */
    private LoanCalculationUtils() {}
    
    /**
     * Calculates monthly payment of the loan.
     * 
     * The formula is: 
     * P = (Pv*R)/(1-(1+R)^(-n))
     *   = (R*Pv(1+R)^n)/((1+R)^n -1)
     * 
     * Where
     * Pv  = Present Value (beginning value or amount of loan)
     * APR = Annual Percentage Rate (one year time period)
     * R   = Periodic Interest Rate = APR/n
     * P   = Monthly Payment
     * n   = total months of interest periods for overall time period 
     */
    public static BigDecimal calculateMonthlyLoanPayment(BigDecimal amount, BigDecimal annualInterestRate, int period) {
        // calculates APR/n
        BigDecimal monthlyInterestRate = annualInterestRate.divide(new BigDecimal(12), 10, RoundingMode.HALF_UP);
        // calculates (1+R)^n
        BigDecimal compound = (monthlyInterestRate.add(BigDecimal.ONE)).pow(period);
        // calculates R*Pv(1+R)^n = R*Pv(compound)
        BigDecimal nominator = (monthlyInterestRate.multiply(amount)).multiply(compound);
        // calculates (1+R)^n -1 = compound - 1
        BigDecimal denominator = compound.subtract(BigDecimal.ONE);
        // calculates and return (R*Pv(1+R)^n)/((1+R)^n -1) = nominator / denominator
        // The result rounded to 2 decimal places
        return nominator.divide(denominator, 2, BigDecimal.ROUND_HALF_UP);
    }
    
    /**
     * Calculates monthly interest payment of the loan.
     * The formula is: 
     * I =  Pv*R
     * 
     * where
     * I = Interest
     * Pv  = Present Value
     * APR = Annual Percentage Rate (one year time period)
     * R   = Periodic Interest Rate = APR/n
     * @return amount of the monthly loan payment.
     */
    public static BigDecimal calculateMonthlyInterest(BigDecimal amount, BigDecimal annualInterestRate) {
        // calculates R = APR/n
        BigDecimal monthlyInterestRate = annualInterestRate.divide(new BigDecimal(12), 10, RoundingMode.HALF_UP);
        // calculate Pv*R
        return amount.multiply(monthlyInterestRate);
    }
    
    /**
     * Calculate the penalty payment
     * The formula is:
     * Pe = Pv * r
     * 
     * where
     * Pe = Penalty 
     * Pv  = Present Value
     * APR = Annual Percentage Rate (one year time period)
     * r   = Periodic Interest Rate = APR/n 
 
     */
    public static BigDecimal calculatePenaltyAmount(LoanAccount loanAccount) {
        BigDecimal penaltyRate = loanAccount.getPenaltyRate();
        BigDecimal amountDue = loanAccount.getRemainingLoan();
        // Interest and penalty calculation uses the same formula
        BigDecimal penaltyFee = calculateMonthlyInterest(amountDue, penaltyRate);
        penaltyFee = penaltyFee.setScale(2, RoundingMode.HALF_UP);
        loanAccount.incrementPenaltyAmount(penaltyFee);
        return penaltyFee;
    }
}
