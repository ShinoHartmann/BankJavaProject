package com.msd.bank.util;

import com.msd.bank.view.HomeScreen;
import javax.swing.JOptionPane;

/**
 * Logger to log debug and error messages.
 */
public class Logger {
    
    /** 
     * logLevel 0 -- for development purpose
     * logLevel 1 -- for production
     */
    private final int logLevel = 0;
    private final String name;
    
    public Logger(Class loggingClass) {
        String classname = loggingClass.getName();
        name = classname.substring(classname.lastIndexOf(".") + 1);
    }
    
    /**
     * This message is the information for the user.
     * Should be shown for developers and customers.
     */
    public void info(String message) {
        System.out.println(name + ": " + message);
        // Do not show error dialog when running unit tests
        if(HomeScreen.MAIN_FRAME_RUNNING) {
            if(logLevel <= 1) {
                JOptionPane.showMessageDialog(null, message);
            }
        }
    }
    
    /**
     * When logLevel is set to be 1, this debug message will not be shown.
     * Uses for debugging.
     */
    public void debug(String message) {
        if(logLevel == 0) {
            System.out.println(name + ": " + message);
        }
    }
    
    public void error(String message) {
        System.err.println(name + ": " + message);
        // Do not show error dialog when running unit tests
        if(HomeScreen.MAIN_FRAME_RUNNING) {
            JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void error(String message, Throwable ex) {
        error(message);
        ex.printStackTrace();
    }
}
