package com.msd.bank.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Provides the current system date/time.
 */
public class TimeProvider implements Cloneable {
    
    private static final String FORMAT = "dd/M/yyyy";
    private static final TimeProvider INSTANCE = new TimeProvider();
    private Calendar today;
    
    private TimeProvider() {}
    
    public static TimeProvider getInstance() {
        return INSTANCE;
    }
    
    public Calendar getToday() {
        if(today != null) {
            return today;
        } else {
            return Calendar.getInstance();
        }
    }
    
    /**
     * The following methods are used for testing purpose and a user can not change the today's date.
     */
    public void setToday(String date) throws ParseException {
        Calendar calendar = createCalendar(date);
        setToday(calendar);
    }
    
    public void setToday(Calendar today) {
        this.today = today;
    }
    
    public static Calendar createCalendar(String date) {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat(FORMAT);
            calendar.setTime(formatter.parse(date));
            return calendar;
        } catch(ParseException ex) {
            throw new RuntimeException("The date format must be '" + FORMAT + "'", ex);
        }
    }
}
