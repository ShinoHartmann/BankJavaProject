package com.msd.bank.view;

import com.msd.bank.controller.*;
import com.msd.bank.model.account.Account;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXDatePicker;
import javax.swing.JLabel;

/**
 * Contains the input restriction and all the settings for all forms of this application.
 * All the forms extends this class.
 */
public abstract class AbstractForm extends JFrame {
    
    private final CustomerController customerController = CustomerController.getInstance();
    private final BaseAccountController baseAccountController = BaseAccountController.getInstance();
    
    public final void showForm() {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setVisible(true);
    }
    
    /**
     * Prevents a user to type other than decimal number for such as amount field.
     */
    protected void checkDecimalNumberInput(KeyEvent evt, JTextField text) {
        try {
            String entered = text.getText() + evt.getKeyChar();
            new BigDecimal(entered);
        }catch(NumberFormatException e){
            evt.consume();
        }
    }
     
    /**
     * Prevents a user to type other than integer numbers into a field.
     */
    protected void checkIntegerInput(KeyEvent evt) {
        char entered = evt.getKeyChar();
        if(!(Character.isDigit(entered))){
            evt.consume();
        }
    }

    protected void clearContent(JComboBox... comboBoxes) {
        for(JComboBox comboBox: comboBoxes) {
            comboBox.setSelectedIndex(0);
        }
    }
    
    protected void clearContent(JLabel... labels) {
        for(JLabel label: labels) {
            label.setText(null);
        }
    }
    
    protected void clearContent(JTextField... textFields) {
        for(JTextField textField: textFields) {
            textField.setText(null);
        }
    }
    
    protected void clearContent(JTable table) {
        ((DynamicTableModel) table.getModel()).clearTableContents();
    }

    /**
     * Gets items from the input field.
     * @return input or selected item converted to String format
     */
    protected String getItem(JComboBox comboBox) {
        if(comboBox.getSelectedItem() == null) {
            return null;
        }
        return comboBox.getSelectedItem().toString();
    }
    
    protected String getItem(JTextField field) {
        return field.getText();
    }
    
    protected Calendar getItem(JXDatePicker dateInput) {
        if(dateInput == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        Date date = dateInput.getDate();
        cal.setTime(date);
        return cal;
    }
    
    /**
     * Fills existing account IDs into a combo box.
     */
    protected void addAccountIdsIntoComboBox(JComboBox comboBox, Class accountClass) {
        String selection = "Select account ID";
        comboBox.addItem(selection);
        for(Account account: baseAccountController.getAccounts()) {
            if(accountClass.isAssignableFrom(account.getClass())) {
                comboBox.addItem(account.getAccountId());
            }
        }
    }
    
    /**
     * Fills existing customer IDs into a combo box.
     */
    protected void addCustomertIdsIntoComboBox(JComboBox comboBox) {
        String selection = "Select customer ID";
        addItemsIntoComboBox(customerController.getCustomerIds(), comboBox, selection);
    }
    
    protected void addItemsIntoComboBox(Set items, JComboBox comboBox, String selection) {
        comboBox.addItem(selection);
        for(Object item: items) {
            comboBox.addItem(item.toString());
        }
    }
    
    protected void updateTable(JTable table) {
        table.updateUI();
    }
    
    /**
     * Set default date for the timePicker.
     */
    protected Calendar setDate(int year, int month, int day) {
        Calendar date = Calendar.getInstance(); 
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, month);
        date.set(Calendar.DAY_OF_MONTH, day);
        return date;
    }
}
