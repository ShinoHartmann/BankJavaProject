package com.msd.bank.view;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * This class is used to create dynamic tables for input forms.
 * The size of the table changes depending on the number of items.
 */
public class DynamicTableModel extends AbstractTableModel {
    
    private final String[] columnNames;
    private final List<Object[]> values = new ArrayList<>();
    
    public DynamicTableModel(String[] columnNames) {
        this.columnNames = columnNames;
    }

    @Override
    public int getRowCount() {
        return values.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if(rowIndex >= values.size()) {
            Object[] row = new Object[columnNames.length];
            values.add(rowIndex, row);
        }
        Object[] row = values.get(rowIndex);
        row[columnIndex] = value;
        fireTableRowsInserted(rowIndex, rowIndex);
    }
    
    public void clearTableContents() {
        values.clear();
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object[] row = values.get(rowIndex);
        return row[columnIndex];
    }    
}
