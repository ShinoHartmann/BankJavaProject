package com.msd.bank.view.account.loan;

import com.msd.bank.util.Logger;
import com.msd.bank.util.InputValidationException;
import com.msd.bank.controller.LoanAccountController;
import com.msd.bank.util.LoanCalculationUtils;
import com.msd.bank.view.AbstractForm;
import java.math.BigDecimal;

public class CreateLoanForm extends AbstractForm {

    private final LoanAccountController controller = LoanAccountController.getInstance();
    private final static Logger LOGGER = new Logger(CreateLoanForm.class);
    
    public CreateLoanForm() {
        initComponents();
        addValuesIntoComboVBox();
        showForm();
    }
    
    private void addValuesIntoComboVBox() {
        addLoanAccountTypeIntoComboBox();
        addCustomertIdsIntoComboBox(customerIdComboBox);
    }
    
    protected void addLoanAccountTypeIntoComboBox() {
        String selection = "Select loan type";
        addItemsIntoComboBox(controller.getAccountTypes(), loanTypeComboBox, selection);
    }
    
    private void clearContents() {
        clearContent(loanTypeComboBox, customerIdComboBox);
        clearContent(accountIdField, amountField, interestField, durationField);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        accountIdField = new javax.swing.JTextField();
        createLoanButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        accountIdLabel = new javax.swing.JLabel();
        amountLabel = new javax.swing.JLabel();
        titleLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        customerIdLabel = new javax.swing.JLabel();
        customerIdComboBox = new javax.swing.JComboBox<>();
        loanTypeComboBox = new javax.swing.JComboBox<>();
        loanTypeLabel = new javax.swing.JLabel();
        interestLabel = new javax.swing.JLabel();
        interestField = new javax.swing.JTextField();
        durationLabel = new javax.swing.JLabel();
        durationField = new javax.swing.JTextField();
        percentageLabel = new javax.swing.JLabel();
        monthLabel = new javax.swing.JLabel();
        calculatePaymentButton = new javax.swing.JButton();
        monthlyPaymentLabel = new javax.swing.JLabel();
        monthlyPaymentResultLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        amountField = new javax.swing.JTextField();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        accountIdField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                accountIdFieldKeyTyped(evt);
            }
        });

        createLoanButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        createLoanButton.setText("Create Loan");
        createLoanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createLoanButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        accountIdLabel.setText("Account ID:");

        amountLabel.setText("Loan Amount:");

        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        titleLabel.setText("CREATE LOAN");

        customerIdLabel.setText("Customer ID:");

        customerIdComboBox.setToolTipText("");

        loanTypeComboBox.setToolTipText("");

        loanTypeLabel.setText("Type of Loan:");

        interestLabel.setText("Annual Interest Rate:");

        interestField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                interestFieldKeyTyped(evt);
            }
        });

        durationLabel.setText("Duration in Month");

        durationField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                durationFieldKeyTyped(evt);
            }
        });

        percentageLabel.setText("%");

        monthLabel.setText("months");

        calculatePaymentButton.setText("Calculate Monthly Payment");
        calculatePaymentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calculatePaymentButtonActionPerformed(evt);
            }
        });

        monthlyPaymentLabel.setText("Monthly Payment: ");

        monthlyPaymentResultLabel.setText(" ");

        jLabel1.setText("£");

        amountField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                amountFieldKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addComponent(cancelButton)
                                .addGap(42, 42, 42)
                                .addComponent(createLoanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(interestLabel)
                                .addGap(28, 28, 28)
                                .addComponent(interestField, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(percentageLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(59, 59, 59)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(monthlyPaymentLabel)
                                        .addGap(18, 18, 18)
                                        .addComponent(monthlyPaymentResultLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(calculatePaymentButton, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(amountLabel)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(amountField, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(45, 45, 45)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(loanTypeLabel)
                                    .addComponent(customerIdLabel)
                                    .addComponent(accountIdLabel))
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(accountIdField, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(durationField, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(monthLabel))
                                    .addComponent(loanTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(customerIdComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addComponent(durationLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(titleLabel)))
                        .addGap(0, 26, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(titleLabel)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loanTypeLabel)
                    .addComponent(loanTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customerIdLabel)
                    .addComponent(customerIdComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(accountIdLabel)
                    .addComponent(accountIdField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(amountLabel)
                    .addComponent(jLabel1)
                    .addComponent(amountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(interestLabel)
                    .addComponent(percentageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(interestField, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(durationField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(monthLabel)
                    .addComponent(durationLabel))
                .addGap(18, 18, 18)
                .addComponent(calculatePaymentButton, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(monthlyPaymentLabel)
                    .addComponent(monthlyPaymentResultLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(createLoanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelButton))
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void createLoanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createLoanButtonActionPerformed
        try {
            controller.createLoanAccount(getItem(loanTypeComboBox), 
                    getItem(accountIdField), 
                    getItem(customerIdComboBox), 
                    getItem(amountField), 
                    getItem(interestField), 
                    getItem(durationField)
            );
            clearContents();
        } catch(InputValidationException ex) {
            LOGGER.error(ex.getMessage());
        } catch(IllegalArgumentException ex) {
            LOGGER.error(ex.getMessage());
        }
    }//GEN-LAST:event_createLoanButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void interestFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_interestFieldKeyTyped
        checkDecimalNumberInput(evt, interestField);
    }//GEN-LAST:event_interestFieldKeyTyped
    
    private void accountIdFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_accountIdFieldKeyTyped
        checkIntegerInput(evt);
    }//GEN-LAST:event_accountIdFieldKeyTyped

    private void durationFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_durationFieldKeyTyped
        checkIntegerInput(evt);
    }//GEN-LAST:event_durationFieldKeyTyped

    private void calculatePaymentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calculatePaymentButtonActionPerformed
        try {
            BigDecimal paymentAmount = controller.calculateMonthlyPayment(
                getItem(amountField), 
                getItem(interestField), 
                getItem(durationField));
        monthlyPaymentResultLabel.setText("£ " + paymentAmount.toString());
        } catch(InputValidationException ex) {
            LOGGER.error(ex.getMessage());
        } 
    }//GEN-LAST:event_calculatePaymentButtonActionPerformed

    private void amountFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_amountFieldKeyTyped
        checkDecimalNumberInput(evt, amountField);
    }//GEN-LAST:event_amountFieldKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField accountIdField;
    private javax.swing.JLabel accountIdLabel;
    private javax.swing.JTextField amountField;
    private javax.swing.JLabel amountLabel;
    private javax.swing.JButton calculatePaymentButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton createLoanButton;
    private javax.swing.JComboBox<String> customerIdComboBox;
    private javax.swing.JLabel customerIdLabel;
    private javax.swing.JTextField durationField;
    private javax.swing.JLabel durationLabel;
    private javax.swing.JTextField interestField;
    private javax.swing.JLabel interestLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JComboBox<String> loanTypeComboBox;
    private javax.swing.JLabel loanTypeLabel;
    private javax.swing.JLabel monthLabel;
    private javax.swing.JLabel monthlyPaymentLabel;
    private javax.swing.JLabel monthlyPaymentResultLabel;
    private javax.swing.JLabel percentageLabel;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
