package com.msd.bank.ui;

import com.msd.bank.util.InputValidationException;
import com.msd.bank.util.TimeProvider;
import com.msd.bank.controller.*;
import com.msd.bank.model.account.*;
import com.msd.bank.model.account.base.*;
import com.msd.bank.model.account.record.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.TreeSet;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.number.OrderingComparison.*;
import static org.hamcrest.CoreMatchers.*;
import static com.msd.bank.model.account.record.TransactionType.*;
import static com.msd.bank.model.account.record.AccountHistoryType.*;

/**
 * This class contains unit tests to test BasicAccountController and AccountController.
 */
public class BaseAccountControllerTest {
    
    private final static String ACCOUNT_ID = "99";
    private final static String ACCOUNT_TYPE = "Current Account";
    private final static String SECOND_ACCOUNT_ID = "2";
    private final static String CUSTOMER_ID ="1";
    private final static String SECOND_CUSTOMER_ID ="2";
    private final static String FIRST_NAME = "Shino";
    private final static String LAST_NAME = "Hartmann";
    private final static String FULL_NAME = FIRST_NAME + " " + LAST_NAME;
    private final static String AMOUNT = "100.00";
    private final static String AMOUNT_500 = "500.00";
    private final static Calendar FIRST_APRIL = TimeProvider.createCalendar("01/04/2016");
    private final static Calendar TENTH_APRIL =  TimeProvider.createCalendar("10/04/2016");
    private final static Calendar ELEVENTH_APRIL =  TimeProvider.createCalendar("11/04/2016");
    private final CustomerController customerController = CustomerController.getInstance();
    private final BaseAccountController baseAccountController = BaseAccountController.getInstance();
    private final AccountStore accountStore = AccountStore.getInstance();
    private final CustomerControllerTest customerTest = new CustomerControllerTest();
    private final TimeProvider timeProvider = TimeProvider.getInstance();
    
    @Before
    public void setUp() {
        timeProvider.setToday(Calendar.getInstance());
        accountStore.clear();
        customerController.clear();
    }
    
    @Test
    public void testOpenAccount() {
        customerTest.testCreateCustomers();
        baseAccountController.createAccount(ACCOUNT_TYPE, ACCOUNT_ID, CUSTOMER_ID);
        baseAccountController.createAccount(ACCOUNT_TYPE, SECOND_ACCOUNT_ID, SECOND_CUSTOMER_ID);
        Account account = baseAccountController.getAccount(ACCOUNT_ID);
        assertThat(account.getOwnerName(), is(FULL_NAME));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotCreateAccountIfAccountIdIsNotInteger() {
        customerTest.testCreateCustomers();
        baseAccountController.createAccount(ACCOUNT_TYPE,"something", CUSTOMER_ID);
        assertThat(baseAccountController.getAccount(ACCOUNT_ID), is(nullValue()));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotCreateAccountIfCustomerIdDoesNotExist() {
        baseAccountController.createAccount(ACCOUNT_TYPE,ACCOUNT_ID, CUSTOMER_ID);
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotCreateAccountsWithSameAccountId() {
        customerTest.testCreateCustomers();
        baseAccountController.createAccount(ACCOUNT_TYPE, ACCOUNT_ID, CUSTOMER_ID);
        baseAccountController.createAccount(ACCOUNT_TYPE, ACCOUNT_ID, SECOND_CUSTOMER_ID);
    }
    
    @Test 
    public void testCloneAccount() {
        testOpenAccount();
        Account account = baseAccountController.getAccount(ACCOUNT_ID);
        Account clonedAccount = account.createClone();
        assertThat(account.getBalance(), comparesEqualTo(clonedAccount.getBalance()));
        assertThat(account, not(clonedAccount));
    }
    
    @Test
    public void testDeposit() {
        testOpenAccount();
        baseAccountController.depositMoney(ACCOUNT_ID, AMOUNT);
        BigDecimal balance = baseAccountController.getAccount(ACCOUNT_ID).getBalance();
        assertThat(balance, comparesEqualTo(new BigDecimal(AMOUNT)));
    }
    
    @Test
    public void testWithdraw() {
        testDeposit();
        String amount = "60";
        baseAccountController.withdrawMoney(ACCOUNT_ID, amount);
        Account account = baseAccountController.getAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        assertThat(balance, comparesEqualTo(new BigDecimal("40")));
    }
    
    @Test 
    public void testOverdraft() {
        testOpenAccount();
        baseAccountController.withdrawMoney(ACCOUNT_ID, AMOUNT_500);  
        BaseAccount account = baseAccountController.getBaseAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        BigDecimal overdraft = account.getOverdraft();
        assertThat(balance, comparesEqualTo(new BigDecimal("500").negate()));
        assertThat(overdraft, comparesEqualTo(new BigDecimal("500")));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotBeAbleToWithdrawMoneyMoreThanAvailableAmount() {
        testOpenAccount();
        baseAccountController.withdrawMoney(ACCOUNT_ID, AMOUNT_500);  
        baseAccountController.withdrawMoney(ACCOUNT_ID, AMOUNT_500);
        baseAccountController.withdrawMoney(ACCOUNT_ID, AMOUNT_500);  
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotBeAbleToWithdrawNegativeValue() {
        testDeposit();
        baseAccountController.withdrawMoney(ACCOUNT_ID, "-100");
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotBeAbleToWithdrawMoneyMoreThanWithdrawalLimit() {
        testDeposit();
        String amount = "550";
        baseAccountController.withdrawMoney(ACCOUNT_ID, amount); 
    }
        
    @Test
    public void testTransferMoney() {
        testDeposit();
        baseAccountController.transferMoney(ACCOUNT_ID, SECOND_ACCOUNT_ID, "40");
        // Test balance of the first account 
        Account account = baseAccountController.getAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        assertThat(balance, comparesEqualTo(new BigDecimal("60")));
        // Test balance of the second account
        account = baseAccountController.getAccount(SECOND_ACCOUNT_ID);
        balance = account.getBalance();
        assertThat(balance, comparesEqualTo(new BigDecimal("40")));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotBeAbleToTransferMoneyMoreThanBalance() {
        testOpenAccount();
        baseAccountController.withdrawMoney(ACCOUNT_ID, AMOUNT_500);
        baseAccountController.withdrawMoney(ACCOUNT_ID, AMOUNT_500);
        String transferAmount = "200";
        // Create second account to transfer
        baseAccountController.transferMoney(ACCOUNT_ID, SECOND_ACCOUNT_ID, transferAmount);
    }
    
    @Test
    public void testRollbackAfterTransferMoney() {
        testTransferMoney();
        baseAccountController.rollback(ACCOUNT_ID);
        baseAccountController.rollback(SECOND_ACCOUNT_ID);
        // Test balance of the first account 
        Account account = baseAccountController.getAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        assertThat(balance, comparesEqualTo(new BigDecimal(AMOUNT)));
        // Test balance of the second account 
        account = baseAccountController.getAccount(SECOND_ACCOUNT_ID);
        balance = account.getBalance();
        assertThat(balance, comparesEqualTo(BigDecimal.ZERO));
    }
    
    @Test
    public void shouldNotShowLastTransactionIfRolledBack() {
        testTransferMoney();
        baseAccountController.rollback(ACCOUNT_ID);
        // Test balance of the first account 
        Account account = baseAccountController.getAccount(ACCOUNT_ID);
        Collection<Transaction> transactions = account.getTransactionsFrom(timeProvider.getToday());
        assertThat(transactions.size(), comparesEqualTo(1));
    }
    
    @Test
    public void testViewBalance() {
        testDeposit();
        assertThat(baseAccountController.viewAccountBalance(ACCOUNT_ID), comparesEqualTo(new BigDecimal(AMOUNT)));
    }
    
    @Test 
    public void testViewTransactions() {
        timeProvider.setToday(FIRST_APRIL);
        testDeposit();
        timeProvider.setToday(TENTH_APRIL);
        baseAccountController.withdrawMoney(ACCOUNT_ID, "50");
        Collection<Record> records =  baseAccountController.getTransactions(ACCOUNT_ID, FIRST_APRIL, TENTH_APRIL);
        Record transaction = records.iterator().next();
        assertThat(transaction.getRecordDate(), is(FIRST_APRIL));
        assertThat(transaction.getType(), is(Deposit.toString()));
        assertThat(records.size(), comparesEqualTo(2));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotUpdateTransactionTableIfInvalidDate() {
        timeProvider.setToday(TENTH_APRIL);
        testWithdraw();
        timeProvider.setToday(FIRST_APRIL);
        Collection<Record> records  = baseAccountController.getTransactions(ACCOUNT_ID, TENTH_APRIL, FIRST_APRIL);
        assertThat(records.isEmpty(), is(true));
    }
    
    @Test 
    public void testViewTransactionBetweenTwoDates() {
        timeProvider.setToday(FIRST_APRIL);
        testDeposit();
        timeProvider.setToday(TENTH_APRIL);
        baseAccountController.withdrawMoney(ACCOUNT_ID, "50");
        timeProvider.setToday(ELEVENTH_APRIL);
        Collection<Record> records = baseAccountController.getTransactions(ACCOUNT_ID, TENTH_APRIL, ELEVENTH_APRIL);       
        Record record = records.iterator().next();
        assertThat(record.getType(), is(Withdraw.toString()));
    }
    
    @Test
    public void testGetAccountHistory() {
        testDeposit();
        Account account = baseAccountController.getAccount(ACCOUNT_ID);
        TreeSet<AccountHistory> history = account.getAccountHistoryByDate(Calendar.getInstance());
        AccountHistory firstHistory = history.first();
        assertThat(firstHistory.getType(), is(OpenAccount.toString()));
    }
    
    @Test
    public void testViewAccountHistory() {
        timeProvider.setToday(FIRST_APRIL);
        testDeposit();
        timeProvider.setToday(TENTH_APRIL);
        baseAccountController.withdrawMoney(ACCOUNT_ID, "50");
        Collection<Record> records = baseAccountController.getAccountRecords(ACCOUNT_ID, FIRST_APRIL, ELEVENTH_APRIL);
        Record record = records.iterator().next();
        assertThat(record.getType(), is(OpenAccount.toString()));
        assertThat(records.size(), comparesEqualTo(3));
    }
    
    @Test
    public void testViewAllAccounts() {
        testAddAccountHolder();
        Collection<BaseAccount> accounts = baseAccountController.getAllBaseAccountsByCustomer(CUSTOMER_ID);
        assertThat(accounts.size(), comparesEqualTo(2));
    }
    
    @Test
    public void testSetOverdraft() {
        testOpenAccount();
        baseAccountController.changeOverdraftLimit(ACCOUNT_ID, AMOUNT);
        BaseAccount account = baseAccountController.getBaseAccount(ACCOUNT_ID);
        assertThat(account.getOverdraftLimit(), comparesEqualTo(new BigDecimal(AMOUNT)));
    }
    
    @Test
    public void testAddAccountHolder() {
        testOpenAccount();
        baseAccountController.addAccountHolder(SECOND_ACCOUNT_ID, CUSTOMER_ID);
        Account account = baseAccountController.getAccount(SECOND_ACCOUNT_ID);
        assertThat(account.getAccountHolder(CUSTOMER_ID), is(customerController.getCustomer(CUSTOMER_ID)));
    }
    
    @Test
    public void testInterestCalculation() {
        timeProvider.setToday(TENTH_APRIL);
        testOpenAccount();
        baseAccountController.depositMoney(ACCOUNT_ID, "10000");
        timeProvider.setToday(ELEVENTH_APRIL);
        baseAccountController.calculateInterestOrPenalty();
        BaseAccount account = baseAccountController.getBaseAccount(ACCOUNT_ID);
        BigDecimal interestAmount = account.getInterestAmount();
        assertThat(interestAmount, comparesEqualTo(new BigDecimal("0.822")));
    }
    
    @Test
    public void shouldNotCalculateInterstOrPenaltyTwiceAday() {
        testInterestCalculation();
        baseAccountController.calculateInterestOrPenalty();
        BaseAccount account = baseAccountController.getBaseAccount(ACCOUNT_ID);
        BigDecimal interestAmount = account.getInterestAmount();
        assertThat(interestAmount, comparesEqualTo(new BigDecimal("0.822")));
    }
    
    @Test
    public void testInterestPayment() {
        timeProvider.setToday(TENTH_APRIL);
        testOpenAccount();
        baseAccountController.depositMoney(ACCOUNT_ID, "10000");
        timeProvider.setToday(ELEVENTH_APRIL);
        baseAccountController.calculateInterestOrPenalty();
        baseAccountController.payInterestAndPenalty();
        BaseAccount account = baseAccountController.getBaseAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        BigDecimal interestAmount = account.getInterestAmount();
        assertThat(balance, comparesEqualTo(new BigDecimal("10000.82")));
        assertThat(interestAmount, comparesEqualTo(new BigDecimal("0")));
    }
    
    @Test
    public void testPenaltyFeeCalculation() {
        timeProvider.setToday(TENTH_APRIL);
        testOpenAccount();
        baseAccountController.withdrawMoney(ACCOUNT_ID, AMOUNT_500);
        timeProvider.setToday(ELEVENTH_APRIL);
        baseAccountController.calculateInterestOrPenalty();
        BaseAccount account = baseAccountController.getBaseAccount(ACCOUNT_ID);
        BigDecimal penaltyAmount = account.getPenaltyAmount();
        BigDecimal interestAmount = account.getInterestAmount();
        assertThat(penaltyAmount, comparesEqualTo(new BigDecimal("6.25")));
        assertThat(interestAmount, comparesEqualTo(BigDecimal.ZERO));
    }
    
    @Test
    public void testPenaltyPayment() {
        testPenaltyFeeCalculation();
        baseAccountController.payInterestAndPenalty();
        BaseAccount account = baseAccountController.getBaseAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        assertThat(balance, comparesEqualTo(new BigDecimal("506.25").negate()));
        BigDecimal penaltyAmount = account.getPenaltyAmount();
        assertThat(penaltyAmount, comparesEqualTo(new BigDecimal("0")));
    }
}
