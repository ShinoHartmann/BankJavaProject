package com.msd.bank.ui;

import com.msd.bank.util.InputValidationException;
import com.msd.bank.controller.*;
import com.msd.bank.model.Customer;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * This class contains unit tests to test the CustomerController.
 */
public class CustomerControllerTest {
    
    private final static String CUSTOMER_ID ="1";
    private final static String SECOND_CUSTOMER_ID ="2";
    private final static String FIRST_NAME = "Shino";
    private final static String LAST_NAME = "Hartmann";
    private final static String FULL_NAME = FIRST_NAME + " " + LAST_NAME;
    private final CustomerController customerController = CustomerController.getInstance();
    private final AccountStore accountStore = AccountStore.getInstance();
    
    @Before
    public void setUp() {
        accountStore.clear();
        customerController.clear();
    }
    
    @Test
    public void testCreateCustomers() {
        customerController.createCustomer(CUSTOMER_ID, FIRST_NAME, LAST_NAME);
        customerController.createCustomer(SECOND_CUSTOMER_ID, "Second", "Holder");
        Customer customer = customerController.getCustomer(CUSTOMER_ID);
        assertThat(customer.getName(), is(FULL_NAME));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotCreateCustomerIfNoAccountName() {
        customerController.createCustomer(CUSTOMER_ID, "", LAST_NAME);
    }
}
