package com.msd.bank.ui;

import com.msd.bank.util.InputValidationException;
import com.msd.bank.controller.*;
import com.msd.bank.model.account.*;
import com.msd.bank.model.account.loan.*;
import com.msd.bank.util.TimeProvider;
import java.math.BigDecimal;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.number.OrderingComparison.*;
import static org.hamcrest.CoreMatchers.*;
import static com.msd.bank.util.LoanCalculationUtils.*;

/**
 * This class contains unit tests to test the LoanAccountController.
 */
public class LoanAccountControllerTest {
    
    private final static String ACC_TYPE = "Current Account";
    private final static String LOAN_ACC_TYPE = "Car Loan Account";    
    private final static String ACCOUNT_ID = "99";
    private final static String SECOND_ACCOUNT_ID = "2";
    private final static String CUSTOMER_ID ="1";
    private final static String SECOND_CUSTOMER_ID ="2";
    private final static String AMOUNT = "100.00";
    private final static Calendar TENTH_APRIL =  TimeProvider.createCalendar("10/04/2016");
    private final static Calendar ELEVENTH_APRIL =  TimeProvider.createCalendar("11/04/2016");
    private final static Calendar TWENTY_MAY = TimeProvider.createCalendar("20/05/2016");
    private final CustomerController customerController = CustomerController.getInstance();
    private final BaseAccountController baseAccountController = BaseAccountController.getInstance();
    private final LoanAccountController loanAccountController = LoanAccountController.getInstance();
    private final AccountStore accountStore = AccountStore.getInstance();
    private final CustomerControllerTest customerTest = new CustomerControllerTest();    
    private final TimeProvider timeProvider = TimeProvider.getInstance();
    
    @Before
    public void setUp() {
        accountStore.clear();
        customerController.clear();
    }
    
    @Test
    public void testOpenLoanAccount() {
        String amount = "10000";
        String interestRate = "3";
        String durationInMonth = "24";
        customerTest.testCreateCustomers();
        loanAccountController.createLoanAccount(LOAN_ACC_TYPE, ACCOUNT_ID, CUSTOMER_ID, amount, interestRate, durationInMonth);
        baseAccountController.createAccount(ACC_TYPE, SECOND_ACCOUNT_ID, SECOND_CUSTOMER_ID);
        Account account = loanAccountController.getAccount(ACCOUNT_ID);
        assertThat(account.getAccountType(), is(LOAN_ACC_TYPE));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotCreateAccountsWithSameAccountId() {
        String amount = "5000";
        String interestRate = "4";
        String durationInMonth = "24";
        testOpenLoanAccount();
        loanAccountController.createLoanAccount(LOAN_ACC_TYPE, ACCOUNT_ID, SECOND_CUSTOMER_ID, amount, interestRate, durationInMonth);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCreateLoanAccountIfAmountExceedsLoanLimit() {
        String amount = "16000";
        String interestRate = "4";
        String durationInMonth = "24";
        customerTest.testCreateCustomers();
        loanAccountController.createLoanAccount(LOAN_ACC_TYPE, ACCOUNT_ID, CUSTOMER_ID, amount, interestRate, durationInMonth);
    }
    
    @Test
    public void testLoanPaymentCalculation() {
        String amount = "10000";
        String interestRate = "3";
        String durationInMonth = "24";
        BigDecimal loanPayment = loanAccountController.calculateMonthlyPayment(amount, interestRate, durationInMonth);
        assertThat(loanPayment, comparesEqualTo(new BigDecimal("429.81")));
    }
    
    @Test
    public void testMakeLumpSumForLoanAccount() {
        testOpenLoanAccount();
        loanAccountController.payLumpSum(ACCOUNT_ID, "5000");
        LoanAccount account = loanAccountController.getLoanAccount(ACCOUNT_ID);
        BigDecimal balance = account.getRemainingLoan();
        assertThat(balance, comparesEqualTo(new BigDecimal("5000")));
    }

    @Test
    public void testMakeMonthlyPaymentForLoanAccount() {
        testOpenLoanAccount();
        loanAccountController.payMonthlyLoan(ACCOUNT_ID);
        LoanAccount account = loanAccountController.getLoanAccount(ACCOUNT_ID);
        BigDecimal balance = account.getRemainingLoan();
        assertThat(account.getdurationInMonth(), comparesEqualTo(23));
        assertThat(balance, comparesEqualTo(new BigDecimal("9595.19")));
    }
    
    @Test
    public void testRecalculateMonthlyPaymentAfterLumpSumPaymentMade() {
        testMakeLumpSumForLoanAccount();
        LoanAccount account = loanAccountController.getLoanAccount(ACCOUNT_ID);
        BigDecimal monthlyPayment = account.getMonthlyPaymentAmount();
        assertThat(monthlyPayment, comparesEqualTo(new BigDecimal("214.91")));
    }
    
    @Test
    public void testInterestCalculation() {
        String amount = "10000";
        String interestRate = "0.03";
        BigDecimal monthlyInterest = calculateMonthlyInterest(new BigDecimal(amount), new BigDecimal(interestRate));
        assertThat(monthlyInterest, comparesEqualTo(new BigDecimal("25")));
    }
    
    @Test
    public void testLoanAccountDeposit() {
        testOpenLoanAccount();
        loanAccountController.depositMoney(ACCOUNT_ID, AMOUNT);
        LoanAccount account = loanAccountController.getLoanAccount(ACCOUNT_ID);
        BigDecimal amountDue = account.getRemainingLoan();
        assertThat(amountDue, comparesEqualTo(new BigDecimal("9900")));
    }
    
    @Test
    public void testTransferMoneyToLoanAccount() {
        testLoanAccountDeposit();
        loanAccountController.depositMoney(SECOND_ACCOUNT_ID, "100");
        loanAccountController.transferMoney(SECOND_ACCOUNT_ID, ACCOUNT_ID, "40");
        // Test balance of the first account 
        Account account = loanAccountController.getAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        BigDecimal amount = new BigDecimal("-9860");
        assertThat(balance, comparesEqualTo(amount));
        // Test balance of loanAccountController second account
        account = loanAccountController.getAccount(SECOND_ACCOUNT_ID);
        balance = account.getBalance();
        assertThat(balance, comparesEqualTo(new BigDecimal(60)));
    }
    
    @Test(expected = InputValidationException.class)
    public void shouldNotBeAbleToTransferMoneyFromLoanAccount() {
        testLoanAccountDeposit();
        loanAccountController.transferMoney(ACCOUNT_ID, SECOND_ACCOUNT_ID, "40");
    }
    
    @Test
    public void testPenaltyPaymentForLoan() {
        timeProvider.setToday(TENTH_APRIL);
        testOpenLoanAccount();
        timeProvider.setToday(TWENTY_MAY);
        loanAccountController.payInterestAndPenalty();
        Account account = loanAccountController.getAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        BigDecimal penaltyAmount = account.getPenaltyAmount();
        assertThat(balance, comparesEqualTo(new BigDecimal("10166.67").negate()));
        assertThat(penaltyAmount, comparesEqualTo(new BigDecimal("0")));
    }
    
    @Test
    public void shouldNotCalculatePenaltyEveryMonth() {
        timeProvider.setToday(TENTH_APRIL);
        testOpenLoanAccount();
        timeProvider.setToday(TWENTY_MAY);
        loanAccountController.calculateInterestOrPenalty();
        LoanAccount account = loanAccountController.getLoanAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        BigDecimal penaltyAmount = account.getPenaltyAmount();
        assertThat(balance, comparesEqualTo(new BigDecimal("10000").negate()));
        assertThat(penaltyAmount, comparesEqualTo(new BigDecimal("0")));
    }
    
    @Test
    public void shouldNotPayPenaltyPaymentIfPaymentMadeInThisMonth() {
        timeProvider.setToday(TENTH_APRIL);
        testMakeMonthlyPaymentForLoanAccount();
        timeProvider.setToday(ELEVENTH_APRIL);
        loanAccountController.payInterestAndPenalty();
        Account account = loanAccountController.getAccount(ACCOUNT_ID);
        BigDecimal balance = account.getBalance();
        BigDecimal penaltyAmount = account.getPenaltyAmount();
        assertThat(balance, comparesEqualTo(new BigDecimal("9595.19").negate()));
        assertThat(penaltyAmount, comparesEqualTo(new BigDecimal("0")));
    }
}
